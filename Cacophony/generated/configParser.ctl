
// generated using Cacophony, an optional module of quasar, see: https://github.com/quasar-team/Cacophony



const string CONNECTIONSETTING_KEY_DRIVER_NUMBER = "DRIVER_NUMBER";
const string CONNECTIONSETTING_KEY_SERVER_NAME = "SERVER_NAME";
const string CONNECTIONSETTING_KEY_SUBSCRIPTION_NAME = "SUBSCRIPTION_NAME";

bool addressConfigWrapper (
    string  dpe,
    string  address,
    int     mode,
    mapping connectionSettings,
    bool active=true
)
{
    string subscription = "";
    if (mode != DPATTR_ADDR_MODE_IO_SQUERY && mode != DPATTR_ADDR_MODE_INPUT_SQUERY)
    {
        subscription = connectionSettings[CONNECTIONSETTING_KEY_SUBSCRIPTION_NAME];
    }
    dyn_string dsExceptionInfo;
    fwPeriphAddress_setOPCUA (
        dpe /*dpe*/,
        connectionSettings[CONNECTIONSETTING_KEY_SERVER_NAME],
        connectionSettings[CONNECTIONSETTING_KEY_DRIVER_NUMBER],
        "ns=2;s="+address,
        subscription /* subscription*/,
        1 /* kind */,
        1 /* variant */,
        750 /* datatype */,
        mode,
        "" /*poll group */,
        dsExceptionInfo
    );
    if (dynlen(dsExceptionInfo)>0)
        return false;
    DebugTN("Setting active on dpe: "+dpe+" to "+active);
    dpSetWait(dpe + ":_address.._active", active);

    return true;
}

bool evaluateActive(
    mapping addressActiveControl,
    string className,
    string varName,
    string dpe)
{
    bool active = false;
    if (mappingHasKey(addressActiveControl, className))
    {
        string regex = addressActiveControl[className];
        int regexMatchResult = regexpIndex(regex, varName, makeMapping("caseSensitive", true));
        DebugTN("The result of evaluating regex: '"+regex+"' with string: '"+varName+" was: "+regexMatchResult);
        if (regexMatchResult>=0)
            active = true;
        else
        {
            active = false;
            DebugN("Note: the address on dpe: "+dpe+" will be non-active because such instructions were passed in the addressActive mapping.");
        }
    }
    else
        active = true; // by default
    return active;
}


bool configurePowerSupplyHMP4040 (
    int     docNum,
    int     childNode,
    string  prefix,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl,
    mapping connectionSettings)
{
    DebugTN("Configure.PowerSupplyHMP4040 called");
    string name;
    xmlGetElementAttribute(docNum, childNode, "name", name);
    string fullName = prefix+name;
    string dpt = "fwLabSetupPowerSupplyHMP4040";

    if (createDps)
    {
        DebugTN("Will create DP "+fullName);
        int result = dpCreate(fullName, dpt);
        if (result != 0)
        {
            DebugTN("dpCreate name='"+fullName+"' dpt='"+dpt+"' not successful or already existing");
            if (!continueOnError)
                throw(makeError("Cacophony", PRIO_SEVERE, ERR_IMPL, 1, "XXX YYY ZZZ"));
        }
    }

    if (assignAddresses)
    {
        string dpe, address;
        dyn_string dsExceptionInfo;
        bool success;
        bool active = false;

        dpe = fullName+".idn";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "PowerSupplyHMP4040",
                     "idn",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".device_descriptor";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "PowerSupplyHMP4040",
                     "device_descriptor",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }


    }

    dyn_int children;
    children = getChildNodesWithName(docNum, childNode, "CommandsPSHMP4040");
    for (int i=1; i<=dynlen(children); i++)
        configureCommandsPSHMP4040 (docNum, children[i], fullName+"/", createDps, assignAddresses, continueOnError, addressActiveControl, connectionSettings);
    children = getChildNodesWithName(docNum, childNode, "OnRequestPSHMP4040");
    for (int i=1; i<=dynlen(children); i++)
        configureOnRequestPSHMP4040 (docNum, children[i], fullName+"/", createDps, assignAddresses, continueOnError, addressActiveControl, connectionSettings);
    children = getChildNodesWithName(docNum, childNode, "ChannelPSHMP4040");
    for (int i=1; i<=dynlen(children); i++)
        configureChannelPSHMP4040 (docNum, children[i], fullName+"/", createDps, assignAddresses, continueOnError, addressActiveControl, connectionSettings);

}


bool configureCommandsPSHMP4040 (
    int     docNum,
    int     childNode,
    string  prefix,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl,
    mapping connectionSettings)
{
    DebugTN("Configure.CommandsPSHMP4040 called");
    string name;
    xmlGetElementAttribute(docNum, childNode, "name", name);
    string fullName = prefix+name;
    string dpt = "fwLabSetupCommandsPSHMP4040";

    if (createDps)
    {
        DebugTN("Will create DP "+fullName);
        int result = dpCreate(fullName, dpt);
        if (result != 0)
        {
            DebugTN("dpCreate name='"+fullName+"' dpt='"+dpt+"' not successful or already existing");
            if (!continueOnError)
                throw(makeError("Cacophony", PRIO_SEVERE, ERR_IMPL, 1, "XXX YYY ZZZ"));
        }
    }

    if (assignAddresses)
    {
        string dpe, address;
        dyn_string dsExceptionInfo;
        bool success;
        bool active = false;

        dpe = fullName+".reset";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "CommandsPSHMP4040",
                     "reset",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".clear";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "CommandsPSHMP4040",
                     "clear",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }


    }

    dyn_int children;

}


bool configureOnRequestPSHMP4040 (
    int     docNum,
    int     childNode,
    string  prefix,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl,
    mapping connectionSettings)
{
    DebugTN("Configure.OnRequestPSHMP4040 called");
    string name;
    xmlGetElementAttribute(docNum, childNode, "name", name);
    string fullName = prefix+name;
    string dpt = "fwLabSetupOnRequestPSHMP4040";

    if (createDps)
    {
        DebugTN("Will create DP "+fullName);
        int result = dpCreate(fullName, dpt);
        if (result != 0)
        {
            DebugTN("dpCreate name='"+fullName+"' dpt='"+dpt+"' not successful or already existing");
            if (!continueOnError)
                throw(makeError("Cacophony", PRIO_SEVERE, ERR_IMPL, 1, "XXX YYY ZZZ"));
        }
    }

    if (assignAddresses)
    {
        string dpe, address;
        dyn_string dsExceptionInfo;
        bool success;
        bool active = false;

        dpe = fullName+".askError";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "OnRequestPSHMP4040",
                     "askError",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".error";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "OnRequestPSHMP4040",
                     "error",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }


    }

    dyn_int children;

}


bool configureChannelPSHMP4040 (
    int     docNum,
    int     childNode,
    string  prefix,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl,
    mapping connectionSettings)
{
    DebugTN("Configure.ChannelPSHMP4040 called");
    string name;
    xmlGetElementAttribute(docNum, childNode, "name", name);
    string fullName = prefix+name;
    string dpt = "fwLabSetupChannelPSHMP4040";

    if (createDps)
    {
        DebugTN("Will create DP "+fullName);
        int result = dpCreate(fullName, dpt);
        if (result != 0)
        {
            DebugTN("dpCreate name='"+fullName+"' dpt='"+dpt+"' not successful or already existing");
            if (!continueOnError)
                throw(makeError("Cacophony", PRIO_SEVERE, ERR_IMPL, 1, "XXX YYY ZZZ"));
        }
    }

    if (assignAddresses)
    {
        string dpe, address;
        dyn_string dsExceptionInfo;
        bool success;
        bool active = false;



    }

    dyn_int children;
    children = getChildNodesWithName(docNum, childNode, "SettingsPSHMP4040");
    for (int i=1; i<=dynlen(children); i++)
        configureSettingsPSHMP4040 (docNum, children[i], fullName+"/", createDps, assignAddresses, continueOnError, addressActiveControl, connectionSettings);
    children = getChildNodesWithName(docNum, childNode, "ReadbackSettingsPSHMP4040");
    for (int i=1; i<=dynlen(children); i++)
        configureReadbackSettingsPSHMP4040 (docNum, children[i], fullName+"/", createDps, assignAddresses, continueOnError, addressActiveControl, connectionSettings);
    children = getChildNodesWithName(docNum, childNode, "ActualPSHMP4040");
    for (int i=1; i<=dynlen(children); i++)
        configureActualPSHMP4040 (docNum, children[i], fullName+"/", createDps, assignAddresses, continueOnError, addressActiveControl, connectionSettings);

}


bool configureSettingsPSHMP4040 (
    int     docNum,
    int     childNode,
    string  prefix,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl,
    mapping connectionSettings)
{
    DebugTN("Configure.SettingsPSHMP4040 called");
    string name;
    xmlGetElementAttribute(docNum, childNode, "name", name);
    string fullName = prefix+name;
    string dpt = "fwLabSetupSettingsPSHMP4040";

    if (createDps)
    {
        DebugTN("Will create DP "+fullName);
        int result = dpCreate(fullName, dpt);
        if (result != 0)
        {
            DebugTN("dpCreate name='"+fullName+"' dpt='"+dpt+"' not successful or already existing");
            if (!continueOnError)
                throw(makeError("Cacophony", PRIO_SEVERE, ERR_IMPL, 1, "XXX YYY ZZZ"));
        }
    }

    if (assignAddresses)
    {
        string dpe, address;
        dyn_string dsExceptionInfo;
        bool success;
        bool active = false;

        dpe = fullName+".i0";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "SettingsPSHMP4040",
                     "i0",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".v0";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "SettingsPSHMP4040",
                     "v0",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".onOff";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "SettingsPSHMP4040",
                     "onOff",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".vProtectionLevel";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "SettingsPSHMP4040",
                     "vProtectionLevel",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".vProtectionMode";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "SettingsPSHMP4040",
                     "vProtectionMode",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".clearTrip";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "SettingsPSHMP4040",
                     "clearTrip",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".fuseDelay";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "SettingsPSHMP4040",
                     "fuseDelay",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".fuseLink";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "SettingsPSHMP4040",
                     "fuseLink",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".fuseUnlink";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "SettingsPSHMP4040",
                     "fuseUnlink",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".fuseOnOff";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "SettingsPSHMP4040",
                     "fuseOnOff",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".cableImpedance";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "SettingsPSHMP4040",
                     "cableImpedance",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }


    }

    dyn_int children;

}


bool configureReadbackSettingsPSHMP4040 (
    int     docNum,
    int     childNode,
    string  prefix,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl,
    mapping connectionSettings)
{
    DebugTN("Configure.ReadbackSettingsPSHMP4040 called");
    string name;
    xmlGetElementAttribute(docNum, childNode, "name", name);
    string fullName = prefix+name;
    string dpt = "fwLabSetupReadbackSettingsPSHMP4040";

    if (createDps)
    {
        DebugTN("Will create DP "+fullName);
        int result = dpCreate(fullName, dpt);
        if (result != 0)
        {
            DebugTN("dpCreate name='"+fullName+"' dpt='"+dpt+"' not successful or already existing");
            if (!continueOnError)
                throw(makeError("Cacophony", PRIO_SEVERE, ERR_IMPL, 1, "XXX YYY ZZZ"));
        }
    }

    if (assignAddresses)
    {
        string dpe, address;
        dyn_string dsExceptionInfo;
        bool success;
        bool active = false;

        dpe = fullName+".i0";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ReadbackSettingsPSHMP4040",
                     "i0",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".v0";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ReadbackSettingsPSHMP4040",
                     "v0",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".onOff";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ReadbackSettingsPSHMP4040",
                     "onOff",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".iRangeMax";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ReadbackSettingsPSHMP4040",
                     "iRangeMax",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".iRangeMin";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ReadbackSettingsPSHMP4040",
                     "iRangeMin",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".vRangeMax";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ReadbackSettingsPSHMP4040",
                     "vRangeMax",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".vRangeMin";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ReadbackSettingsPSHMP4040",
                     "vRangeMin",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".vProtectionLevel";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ReadbackSettingsPSHMP4040",
                     "vProtectionLevel",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".vProtectionMode";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ReadbackSettingsPSHMP4040",
                     "vProtectionMode",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".fuseDelay";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ReadbackSettingsPSHMP4040",
                     "fuseDelay",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".fuseLink";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ReadbackSettingsPSHMP4040",
                     "fuseLink",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".fuseOnOff";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ReadbackSettingsPSHMP4040",
                     "fuseOnOff",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }


    }

    dyn_int children;

}


bool configureActualPSHMP4040 (
    int     docNum,
    int     childNode,
    string  prefix,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl,
    mapping connectionSettings)
{
    DebugTN("Configure.ActualPSHMP4040 called");
    string name;
    xmlGetElementAttribute(docNum, childNode, "name", name);
    string fullName = prefix+name;
    string dpt = "fwLabSetupActualPSHMP4040";

    if (createDps)
    {
        DebugTN("Will create DP "+fullName);
        int result = dpCreate(fullName, dpt);
        if (result != 0)
        {
            DebugTN("dpCreate name='"+fullName+"' dpt='"+dpt+"' not successful or already existing");
            if (!continueOnError)
                throw(makeError("Cacophony", PRIO_SEVERE, ERR_IMPL, 1, "XXX YYY ZZZ"));
        }
    }

    if (assignAddresses)
    {
        string dpe, address;
        dyn_string dsExceptionInfo;
        bool success;
        bool active = false;

        dpe = fullName+".isOn";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ActualPSHMP4040",
                     "isOn",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".trip";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ActualPSHMP4040",
                     "trip",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".iMon";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ActualPSHMP4040",
                     "iMon",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".vMon";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ActualPSHMP4040",
                     "vMon",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".fuseTrip";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ActualPSHMP4040",
                     "fuseTrip",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }
        dpe = fullName+".vLoad";
        address = dpe; // address can be generated from dpe after some mods ...
        strreplace(address, "/", ".");

        active = evaluateActive(
                     addressActiveControl,
                     "ActualPSHMP4040",
                     "vLoad",
                     dpe);

        success = addressConfigWrapper(
                      dpe,
                      address,
                      DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                      connectionSettings,
                      active);

        if (!success)
        {
            DebugTN("Failed setting address "+address+"; will terminate now.");
            return false;
        }


    }

    dyn_int children;

}


dyn_int getChildNodesWithName (int docNum, int parentNode, string name)
{
    dyn_int result;
    int node = xmlFirstChild(docNum, parentNode);
    while (node >= 0)
    {
        if (xmlNodeName(docNum, node)==name)
            dynAppend(result, node);
        node = xmlNextSibling (docNum, node);
    }
    return result;
}

int parseConfig (
    string  configFileName,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl = makeMapping(),
    mapping connectionSettings = makeMapping())
/* Create instances */
{

    /* Apply defaults in connectionSettings, when not concretized by the user */
    if (!mappingHasKey(connectionSettings, CONNECTIONSETTING_KEY_DRIVER_NUMBER))
    {
        connectionSettings[CONNECTIONSETTING_KEY_DRIVER_NUMBER] = 10;
    }
    if (!mappingHasKey(connectionSettings, CONNECTIONSETTING_KEY_SERVER_NAME))
    {
        connectionSettings[CONNECTIONSETTING_KEY_SERVER_NAME] = "OPCUA_LABSETUP";
    }
    if (!mappingHasKey(connectionSettings, CONNECTIONSETTING_KEY_SUBSCRIPTION_NAME))
    {
        connectionSettings[CONNECTIONSETTING_KEY_SUBSCRIPTION_NAME] = "OPCUA_LABSETUP_DefaultSubscription";
    }

    /* Pre/Suffix the expression with ^$ to enable exact matches and also check if given patterns make sense */
    for (int i=1; i<=mappinglen(addressActiveControl); i++)
    {
        string regexp = mappingGetValue(addressActiveControl, i);
        regexp = "^"+regexp+"$";
        addressActiveControl[mappingGetKey(addressActiveControl, i)] = regexp;
        int regexpResult = regexpIndex(regexp, "thisdoesntmatter");
        if (regexpResult <= -2)
        {
            DebugTN("It seems that the given regular expression is wrong: "+regexp+"    the process will be aborted");
            return -1;
        }
    }

    string errMsg;
    int errLine;
    int errColumn;

    string configFileToLoad = configFileName;

    if (! _UNIX)
    {
        DebugTN("This code was validated only on Linux systems. For Windows, BE-ICS should perform the validation and release the component. See at https://its.cern.ch/jira/browse/OPCUA-1519 for more information.");
        return -1;
    }

    // try to perform entity substitution
    string tempFile = configFileToLoad + ".temp";
    int result = system("xmllint --noent " + configFileToLoad + " > " + tempFile);
    DebugTN("The call to 'xmllint --noent' resulted in: "+result);
    if (result != 0)
    {
        DebugTN("It was impossible to run xmllint to inflate entities. WinCC OA might load this file incorrectly if entity references are used. So we decided it wont be possible. See at https://its.cern.ch/jira/browse/OPCUA-1519 for more information.");
        return -1;
    }
    configFileToLoad = tempFile;

    int docNum = xmlDocumentFromFile(configFileToLoad, errMsg, errLine, errColumn);
    if (docNum < 0)
    {
        DebugN("Didn't open the file: at Line="+errLine+" Column="+errColumn+" Message=" + errMsg);
        return -1;
    }

    int firstNode = xmlFirstChild(docNum);
    if (firstNode < 0)
    {
        DebugN("Cant get the first child of the config file.");
        return -1;
    }

    while (xmlNodeName(docNum, firstNode) != "configuration")
    {
        firstNode = xmlNextSibling(docNum, firstNode);
        if (firstNode < 0)
        {
            DebugTN("configuration node not found, sorry.");
            return -1;
        }
    }

    // now firstNode holds configuration node
    dyn_int children;
    dyn_int children = getChildNodesWithName(docNum, firstNode, "PowerSupplyHMP4040");
    for (int i = 1; i<=dynlen(children); i++)
    {
        configurePowerSupplyHMP4040 (docNum, children[i], "", createDps, assignAddresses, continueOnError, addressActiveControl, connectionSettings);
    }


    return 0;
}