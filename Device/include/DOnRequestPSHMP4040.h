
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.

    The stub of this file was generated by quasar (https://github.com/quasar-team/quasar/)

    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.


 */


#ifndef __DOnRequestPSHMP4040__H__
#define __DOnRequestPSHMP4040__H__

#include <vector>                  // TODO; should go away, is already in Base class for ages
#include <boost/thread/mutex.hpp>  // TODO; should go away, is already in Base class for ages

#include <statuscode.h>            // TODO; should go away, is already in Base class for ages
#include <uadatetime.h>            // TODO; should go away, is already in Base class for ages
#include <session.h>               // TODO; should go away, is already in Base class for ages

#include <DRoot.h>                 // TODO; should go away, is already in Base class for ages
#include <Configuration.hxx>       // TODO; should go away, is already in Base class for ages

#include <Base_DOnRequestPSHMP4040.h>

namespace Device
{

class
    DOnRequestPSHMP4040
    : public Base_DOnRequestPSHMP4040
{

public:
    /* sample constructor */
    explicit DOnRequestPSHMP4040 (
        const Configuration::OnRequestPSHMP4040& config,
        Parent_DOnRequestPSHMP4040* parent
    ) ;
    /* sample dtr */
    ~DOnRequestPSHMP4040 ();

    /* delegators for
    cachevariables and sourcevariables */
    /* Note: never directly call this function. */
    UaStatus writeAskError ( const OpcUa_Boolean& v);


    /* delegators for methods */

private:
    /* Delete copy constructor and assignment operator */
    DOnRequestPSHMP4040( const DOnRequestPSHMP4040& other );
    DOnRequestPSHMP4040& operator=(const DOnRequestPSHMP4040& other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
      void restoreOnRequestToFalse();

private:
      //last values
      bool l_askError = 0;
      std::string l_error = "";



};

}

#endif // __DOnRequestPSHMP4040__H__
