
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.

    The stub of this file was generated by quasar (https://github.com/quasar-team/quasar/)

    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.


 */


#ifndef __DSettingsPSHMP4040__H__
#define __DSettingsPSHMP4040__H__

#include <vector>                  // TODO; should go away, is already in Base class for ages
#include <boost/thread/mutex.hpp>  // TODO; should go away, is already in Base class for ages

#include <statuscode.h>            // TODO; should go away, is already in Base class for ages
#include <uadatetime.h>            // TODO; should go away, is already in Base class for ages
#include <session.h>               // TODO; should go away, is already in Base class for ages

#include <DRoot.h>                 // TODO; should go away, is already in Base class for ages
#include <Configuration.hxx>       // TODO; should go away, is already in Base class for ages

#include <Base_DSettingsPSHMP4040.h>
#include <DReadbackSettingsPSHMP4040.h>
#include <DActualPSHMP4040.h>

namespace Device
{

class
    DSettingsPSHMP4040
    : public Base_DSettingsPSHMP4040
{

public:
    /* sample constructor */
    explicit DSettingsPSHMP4040 (
        const Configuration::SettingsPSHMP4040& config,
        Parent_DSettingsPSHMP4040* parent
    ) ;
    /* sample dtr */
    ~DSettingsPSHMP4040 ();

    /* delegators for
    cachevariables and sourcevariables */
    /* Note: never directly call this function. */
    UaStatus writeI0 ( const OpcUa_Double& v);
    /* Note: never directly call this function. */
    UaStatus writeV0 ( const OpcUa_Double& v);
    /* Note: never directly call this function. */
    UaStatus writeOnOff ( const OpcUa_Boolean& v);
    /* Note: never directly call this function. */
    UaStatus writeVProtectionLevel ( const OpcUa_Double& v);
    /* Note: never directly call this function. */
    UaStatus writeVProtectionMode ( const UaString& v);
    /* Note: never directly call this function. */
    UaStatus writeClearTrip ( const OpcUa_Boolean& v);
    /* Note: never directly call this function. */
    UaStatus writeFuseDelay ( const OpcUa_UInt32& v);
    /* Note: never directly call this function. */
    UaStatus writeFuseLink ( const OpcUa_UInt32& v);
    /* Note: never directly call this function. */
    UaStatus writeFuseUnlink ( const OpcUa_UInt32& v);
    /* Note: never directly call this function. */
    UaStatus writeFuseOnOff ( const OpcUa_Boolean& v);
    /* Note: never directly call this function. */
    UaStatus writeCableImpedance ( const OpcUa_Double& v);


    /* delegators for methods */

private:
    /* Delete copy constructor and assignment operator */
    DSettingsPSHMP4040( const DSettingsPSHMP4040& other );
    DSettingsPSHMP4040& operator=(const DSettingsPSHMP4040& other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
      void restoreSettingsToFalse();
      void initialize();

private:
      //last values
      float l_i0 = 0, l_v0 = 0, l_vProtLevel=0, l_cableImpedance=0;
      bool l_onOff = 0, l_fuseOnOff=0, l_clearTrip = 0;
      std::string l_vProtMode = "";
      int l_fuseDelay = -1, l_fuseLink = -1, l_fuseUnlink = -1;

      //corresponding ReadbackSettings and Actual device class,
      //intialized in DSettingsPSHMP4040::initialize() function
      Device::DReadbackSettingsPSHMP4040 *readback;  
      Device::DActualPSHMP4040 *actual;  


};

}

#endif // __DSettingsPSHMP4040__H__
