

// generated using Cacophony, an optional module of quasar, see: https://github.com/quasar-team/Cacophony


//PowerSupplyHMP4040
bool createDptPowerSupplyHMP4040()
{
    // the names of vars and the way of generating DPT come directly from examples of dpTypeCreate
    dyn_dyn_string xxdepes;
    dyn_dyn_int xxdepei;
    dynAppend(xxdepes, makeDynString("fwLabSetupPowerSupplyHMP4040", ""));
    dynAppend(xxdepei, makeDynInt(DPEL_STRUCT));
    dynAppend(xxdepes, makeDynString("", "idn"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "device_descriptor"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));



    int status = dpTypeCreate(xxdepes, xxdepei);
    return status == 0;
}

//CommandsPSHMP4040
bool createDptCommandsPSHMP4040()
{
    // the names of vars and the way of generating DPT come directly from examples of dpTypeCreate
    dyn_dyn_string xxdepes;
    dyn_dyn_int xxdepei;
    dynAppend(xxdepes, makeDynString("fwLabSetupCommandsPSHMP4040", ""));
    dynAppend(xxdepei, makeDynInt(DPEL_STRUCT));
    dynAppend(xxdepes, makeDynString("", "reset"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "clear"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));



    int status = dpTypeCreate(xxdepes, xxdepei);
    return status == 0;
}

//OnRequestPSHMP4040
bool createDptOnRequestPSHMP4040()
{
    // the names of vars and the way of generating DPT come directly from examples of dpTypeCreate
    dyn_dyn_string xxdepes;
    dyn_dyn_int xxdepei;
    dynAppend(xxdepes, makeDynString("fwLabSetupOnRequestPSHMP4040", ""));
    dynAppend(xxdepei, makeDynInt(DPEL_STRUCT));
    dynAppend(xxdepes, makeDynString("", "askError"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "error"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));



    int status = dpTypeCreate(xxdepes, xxdepei);
    return status == 0;
}

//ChannelPSHMP4040
bool createDptChannelPSHMP4040()
{
    // the names of vars and the way of generating DPT come directly from examples of dpTypeCreate
    dyn_dyn_string xxdepes;
    dyn_dyn_int xxdepei;
    dynAppend(xxdepes, makeDynString("fwLabSetupChannelPSHMP4040", ""));
    dynAppend(xxdepei, makeDynInt(DPEL_STRUCT));



    int status = dpTypeCreate(xxdepes, xxdepei);
    return status == 0;
}

//SettingsPSHMP4040
bool createDptSettingsPSHMP4040()
{
    // the names of vars and the way of generating DPT come directly from examples of dpTypeCreate
    dyn_dyn_string xxdepes;
    dyn_dyn_int xxdepei;
    dynAppend(xxdepes, makeDynString("fwLabSetupSettingsPSHMP4040", ""));
    dynAppend(xxdepei, makeDynInt(DPEL_STRUCT));
    dynAppend(xxdepes, makeDynString("", "i0"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "v0"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "onOff"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "vProtectionLevel"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "vProtectionMode"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "clearTrip"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "fuseDelay"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_UINT));
    dynAppend(xxdepes, makeDynString("", "fuseLink"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_UINT));
    dynAppend(xxdepes, makeDynString("", "fuseUnlink"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_UINT));
    dynAppend(xxdepes, makeDynString("", "fuseOnOff"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "cableImpedance"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));



    int status = dpTypeCreate(xxdepes, xxdepei);
    return status == 0;
}

//ReadbackSettingsPSHMP4040
bool createDptReadbackSettingsPSHMP4040()
{
    // the names of vars and the way of generating DPT come directly from examples of dpTypeCreate
    dyn_dyn_string xxdepes;
    dyn_dyn_int xxdepei;
    dynAppend(xxdepes, makeDynString("fwLabSetupReadbackSettingsPSHMP4040", ""));
    dynAppend(xxdepei, makeDynInt(DPEL_STRUCT));
    dynAppend(xxdepes, makeDynString("", "i0"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "v0"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "onOff"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "iRangeMax"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "iRangeMin"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "vRangeMax"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "vRangeMin"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "vProtectionLevel"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "vProtectionMode"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "fuseDelay"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_UINT));
    dynAppend(xxdepes, makeDynString("", "fuseLink"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_UINT));
    dynAppend(xxdepes, makeDynString("", "fuseOnOff"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));



    int status = dpTypeCreate(xxdepes, xxdepei);
    return status == 0;
}

//ActualPSHMP4040
bool createDptActualPSHMP4040()
{
    // the names of vars and the way of generating DPT come directly from examples of dpTypeCreate
    dyn_dyn_string xxdepes;
    dyn_dyn_int xxdepei;
    dynAppend(xxdepes, makeDynString("fwLabSetupActualPSHMP4040", ""));
    dynAppend(xxdepei, makeDynInt(DPEL_STRUCT));
    dynAppend(xxdepes, makeDynString("", "isOn"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "trip"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "iMon"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "vMon"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "fuseTrip"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "vLoad"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));



    int status = dpTypeCreate(xxdepes, xxdepei);
    return status == 0;
}

int createDpts (string dptFilter=".*")
{
    {
        int result = regexpIndex(dptFilter, "PowerSupplyHMP4040");
        if (result >= 0)
        {
            if (!createDptPowerSupplyHMP4040())
                return 1;
        }
        else
        {
            DebugN("DPT PowerSupplyHMP4040 not covered by provided dptFilter, skipping");
        }
    }
    {
        int result = regexpIndex(dptFilter, "CommandsPSHMP4040");
        if (result >= 0)
        {
            if (!createDptCommandsPSHMP4040())
                return 1;
        }
        else
        {
            DebugN("DPT CommandsPSHMP4040 not covered by provided dptFilter, skipping");
        }
    }
    {
        int result = regexpIndex(dptFilter, "OnRequestPSHMP4040");
        if (result >= 0)
        {
            if (!createDptOnRequestPSHMP4040())
                return 1;
        }
        else
        {
            DebugN("DPT OnRequestPSHMP4040 not covered by provided dptFilter, skipping");
        }
    }
    {
        int result = regexpIndex(dptFilter, "ChannelPSHMP4040");
        if (result >= 0)
        {
            if (!createDptChannelPSHMP4040())
                return 1;
        }
        else
        {
            DebugN("DPT ChannelPSHMP4040 not covered by provided dptFilter, skipping");
        }
    }
    {
        int result = regexpIndex(dptFilter, "SettingsPSHMP4040");
        if (result >= 0)
        {
            if (!createDptSettingsPSHMP4040())
                return 1;
        }
        else
        {
            DebugN("DPT SettingsPSHMP4040 not covered by provided dptFilter, skipping");
        }
    }
    {
        int result = regexpIndex(dptFilter, "ReadbackSettingsPSHMP4040");
        if (result >= 0)
        {
            if (!createDptReadbackSettingsPSHMP4040())
                return 1;
        }
        else
        {
            DebugN("DPT ReadbackSettingsPSHMP4040 not covered by provided dptFilter, skipping");
        }
    }
    {
        int result = regexpIndex(dptFilter, "ActualPSHMP4040");
        if (result >= 0)
        {
            if (!createDptActualPSHMP4040())
                return 1;
        }
        else
        {
            DebugN("DPT ActualPSHMP4040 not covered by provided dptFilter, skipping");
        }
    }
    return 0;
}