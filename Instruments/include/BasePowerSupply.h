/*
 * BasePowerSupply.h
 *
 *  Created on: Nov 12, 2020
 *      Author: ressegotti
 */

#include <string>
#include <sstream>
#include <iostream>
#include <LogIt.h>
#include "ScpiUtils.h"


#ifndef INSTRUMENTS_BASEPOWERSUPPLY_H_
#define INSTRUMENTS_BASEPOWERSUPPLY_H_

namespace Instruments {

class BasePowerSupply {
public:
	BasePowerSupply();
	virtual ~BasePowerSupply();

public:
	int initialize (std::string, int =1);
	int m_dev = -1;
	std::string m_idn = "";

	// applying new settings
	// arguments: (newsetting, targetChannel)
	int setVoltage(float, int=1);
	int setCurrent(float, int=1);
	int setOnoff(bool, int=1);

	// getting device settings
	// arguments: (readbacksetting, targetChannel)
	int readVoltage(float&, int=1);
	int readCurrent(float&, int=1);
	int readOnoff(bool&, int=1);
	int readImax(float&, int=-1);
	int readImin(float&, int=-1);
	int readVmax(float&, int=-1);
	int readVmin(float&, int=-1);

	// actual values
	// arguments: (returnvalue, targetChannel)
	int actualIson(bool&i, int=1);
	int actualTrip(bool&, int=-1);
	int actualCurrent(float&, int=1);
	int actualVoltage(float&, int=1);

	// commands
	int sendReset();
	int sendClear();
	int clearTrip(int=-1);

	// values "on request"
	int askError(std::string&);

public:
	Scpi::ScpiUtils * m_comm = new Scpi::ScpiUtils();

public:
bool is_E3631A = false;   //Agilent/HP/Keysight E3631A 
bool is_E3648A = false;   //Agilent E3648A 

};


} /* namespace Instruments */

#endif /* BASEPOWERSUPPLY_H_ */
