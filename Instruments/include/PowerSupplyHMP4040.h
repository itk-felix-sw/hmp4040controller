/*
 * PowerSupplyHMP4040.h
 *
 *  Created on: Nov 12, 2020
 *      Author: ressegotti
 */

#include "BasePowerSupply.h"


#ifndef INSTRUMENTS_POWERSUPPLY2_H_
#define INSTRUMENTS_POWERSUPPLY2_H_

namespace Instruments {

class PowerSupplyHMP4040: public BasePowerSupply {
public:
	PowerSupplyHMP4040();
	virtual ~PowerSupplyHMP4040();

public:

	// applying new settings
	// arguments: (newsetting, targetChannel)
	int setVprotectionLevel(float, int=1); 
	int setVprotectionMode(std::string, int=1); 
	int setFuseDelay(int, int=1);
	int setFuseLink(int, int=1);
	int setFuseUnlink(int, int=1);
	int setFuseState(bool, int=1);

	// getting device settings
	// arguments: (readbacksetting, targetChannel)
	int readVprotectionLevel(float&, int=1);
	int readVprotectionMode(std::string&, int=1);
	int readFuseDelay(int&, int=1);
	int readFuseLink(int, bool&, int=1);
	int readFuseState(bool&, int=1);

	// actual values
	// arguments: (returnvalue, targetChannel)
	int actualFuseTrip(bool&, int=1);


};


} /* namespace Instruments */

#endif /* POWERSUPPLY2_H_ */
