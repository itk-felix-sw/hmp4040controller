/*
 * BasePowerSupply.cpp
 *
 *  Created on: Nov 12, 2020
 *      Author: ressegotti
 */
// This is a collection of minimal SCPI commands for power supplies
// to be inherited in other more extended classes of power supplies

#include "BasePowerSupply.h"

namespace Instruments {

BasePowerSupply::BasePowerSupply() {
	// TODO Auto-generated constructor stub
}

BasePowerSupply::~BasePowerSupply() {
	// TODO Auto-generated destructor stub
}

int BasePowerSupply::initialize (std::string connString, int maxCh	) {
	//initialize communication
	m_comm->initialize(connString, maxCh);	

    m_idn = m_comm->m_idn;
    m_dev = m_comm->m_dev;

  //Agilent/HP/Keysight E3631A
    if (m_idn.find("E3631A") != std::string::npos) is_E3631A = true; 
    if (m_idn.find("E3648A") != std::string::npos) is_E3648A = true; 

    return m_dev;
}


// applying new settings
int BasePowerSupply::setVoltage(float val, int ch) {
	return m_comm->sendCommandInval("VOLT", val, ch);
}
int BasePowerSupply::setCurrent(float val, int ch) {
	return m_comm->sendCommandInval("CURR", val, ch);
}
int BasePowerSupply::setOnoff(bool onoff, int ch) {
	return m_comm->sendCommandInval("OUTP", onoff, ch);
}

// getting device settings
int BasePowerSupply::readVoltage(float &Vset, int ch) {
	return m_comm->sendCommandRetval("VOLT?", Vset, ch);
}
int BasePowerSupply::readCurrent(float &Iset, int ch) {
	return m_comm->sendCommandRetval("CURR?", Iset, ch);
}
int BasePowerSupply::readOnoff(bool &onoff, int ch) {
	return m_comm->sendCommandRetval("OUTP?", onoff, ch);
}
int BasePowerSupply::readImax(float &iMax, int ch) {
	return m_comm->sendCommandRetval("CURR? max", iMax, ch);
}
int BasePowerSupply::readImin(float &iMin, int ch) {
	return m_comm->sendCommandRetval("CURR? min", iMin, ch);
}
int BasePowerSupply::readVmax(float &vMax, int ch) {
	return m_comm->sendCommandRetval("VOLT? max", vMax, ch);
}
int BasePowerSupply::readVmin(float &vMin, int ch) {
	return m_comm->sendCommandRetval("VOLT? min", vMin, ch);
}

// actual values
int BasePowerSupply::actualIson(bool &isOn, int ch) {
	return m_comm->sendCommandRetval("OUTP?", isOn, ch);
}
int BasePowerSupply::actualTrip(bool &isTrip, int ch) {
	if (is_E3631A) return 0;
	return m_comm->sendCommandRetval("VOLT:PROT:TRIP?", isTrip, ch);
}
int BasePowerSupply::actualCurrent(float &iMon, int ch) {
	return m_comm->sendCommandRetval("MEAS:CURR?", iMon, ch);
}
int BasePowerSupply::actualVoltage(float &vMon, int ch) {
	return m_comm->sendCommandRetval("MEAS:VOLT?", vMon, ch);
}

// commands
int BasePowerSupply::sendReset() {
	return m_comm->sendCommand("*RST");
}

int BasePowerSupply::sendClear() {
	return m_comm->sendCommand("*CLS");
}

int BasePowerSupply::clearTrip(int ch) {
	if (is_E3631A) return 0;
	return m_comm->sendCommand("VOLT:PROT:clear", ch);
}

// values on request
int BasePowerSupply::askError(std::string& error) {
	return m_comm->sendCommandRetval("SYST:ERR?", error);
}
} /* namespace Instruments */
