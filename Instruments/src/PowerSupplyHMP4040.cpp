/*
 * PowerSupplyHMP4040.cpp
 *
 *  Created on: Nov 12, 2020
 *      Author: ressegotti
 */
// This collection of SCPI commands is compatible with:
// Rohde&Schwarz HMP4040

#include "PowerSupplyHMP4040.h"

namespace Instruments {

PowerSupplyHMP4040::PowerSupplyHMP4040() {
	// TODO Auto-generated constructor stub

}

PowerSupplyHMP4040::~PowerSupplyHMP4040() {
	// TODO Auto-generated destructor stub
}



// applying new settings
int PowerSupplyHMP4040::setVprotectionLevel(float val, int ch) {
	return m_comm->sendCommandInval("VOLT:PROT:LEV", val, ch);
}

int PowerSupplyHMP4040::setVprotectionMode(std::string mode, int ch) {
	//R&S HMP4040 accepts MEASured|PROTected
	return m_comm->sendCommandInval("VOLT:PROT:MODE", mode, ch);
}
int PowerSupplyHMP4040::setFuseDelay(int delay, int ch) {
	return m_comm->sendCommandInval("FUSE:DEL", delay, ch);
}
int PowerSupplyHMP4040::setFuseLink(int link, int ch) {
	//e.g. setFuseLink(2, 1)
	//selects channel1, link channl2 fuse to it:
	return m_comm->sendCommandInval("FUSE:LINK", link, ch);
}
int PowerSupplyHMP4040::setFuseUnlink(int linkch, int ch) {
	//dissolves link to specific fuse on selected channel
	//e.g. readFuseUnlink(2, answ, 1)
	//selects channel1, unlink channel2 from it
	return m_comm->sendCommandInval("FUSE:UNLink", linkch, ch);
}
int PowerSupplyHMP4040::setFuseState(bool state, int ch) {
	return m_comm->sendCommandInval("FUSE:STAT", state, ch);
}

// getting device settings
int PowerSupplyHMP4040::readVprotectionLevel(float &level, int ch) {
	return m_comm->sendCommandRetval("VOLT:PROT?", level, ch);
}
int PowerSupplyHMP4040::readVprotectionMode(std::string &mode, int ch) {
	return m_comm->sendCommandRetval("VOLT:PROT:MODE?", mode, ch);
}
int PowerSupplyHMP4040::readFuseDelay(int &delay, int ch) {
	return m_comm->sendCommandRetval("FUSE:DEL?", delay, ch);
}
int PowerSupplyHMP4040::readFuseLink(int linkch, bool &answ, int ch) {
	//e.g. readFuseLink(2, answ, 1)
	//selects channel1, asks if channel2 is linked to it:
	//it returns answ=1 if fuse2 is linked to channel1,
	//it returns answ=0 if fuse2 is not linked to channel1
	return m_comm->sendCommandInvalRetval("FUSE:LINK?", linkch, answ, ch);
}
int PowerSupplyHMP4040::readFuseState(bool &state, int ch) {
	return m_comm->sendCommandRetval("FUSE:STAT?", state, ch);
}

// actual values
int PowerSupplyHMP4040::actualFuseTrip(bool &trip, int ch) {
	return m_comm->sendCommandRetval("FUSE:TRIP?", trip, ch);
}


} /* namespace Instruments */
