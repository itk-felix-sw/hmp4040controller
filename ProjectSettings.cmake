# Please see Documentation/quasarBuildSystem.html for
# information how to use this file.

# TADQ envinronment
if(NOT DEFINED ENV{BOOST__HOME})
set(ENV{BOOST__HOME} $ENV{LCG_INST_PATH}/$ENV{TDAQ_LCG_RELEASE}/Boost/1.77.0/$ENV{CMTCONFIG})
message("BOOST__HOME=$ENV{BOOST__HOME}")
endif()
if(NOT DEFINED ENV{XERCESC__HOME})
set(ENV{XERCESC__HOME} $ENV{LCG_INST_PATH}/$ENV{TDAQ_LCG_RELEASE}/XercesC/3.2.3/$ENV{CMTCONFIG})
message("XERCESC__HOME=$ENV{XERCESC__HOME}")
endif()

set(CUSTOM_SERVER_MODULES Instruments)
set(EXECUTABLE OpcUaServer)
set(SERVER_INCLUDE_DIRECTORIES InfluxDBWrite/include Scpi/include SerialTransport/include $ENV{XERCESC__HOME}/include $ENV{TDAQC_INST_PATH}/include $ENV{TDAQ_INST_PATH}/include $ENV{TDAQ_INST_PATH}/$ENV{CMTCONFIG}/include $ENV{ITK_INST_PATH}/include)
#set(SERVER_LINK_LIBRARIES -lgpib -lcurl -lpthread -lpaho-mqtt3as)
#set(SERVER_LINK_LIBRARIES -lcurl -lpthread boost_program_options boost_system-mt)
set(SERVER_LINK_LIBRARIES -lpthread boost_program_options boost_system-mt -lboost_system -lboost_thread)
set(SERVER_LINK_DIRECTORIES $ENV{BOOST__HOME}/lib $ENV{XERCESC__HOME}/lib )


#set(SERVER_INCLUDE_DIRECTORIES $ENV{XERCESC__HOME}/include )

set(IGNORE_DEFAULT_BOOST_SETUP ON)

##
## If ON, in addition to an executable, a shared object will be created.
##
set(BUILD_SERVER_SHARED_LIB OFF)

##
## Add here any additional boost libraries needed with their canonical name
## examples: date_time atomic etc.
## Note: boost paths are resolved either from $BOOST_ROOT if defined or system paths as fallback
##
set(ADDITIONAL_BOOST_LIBS )
