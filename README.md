# QuasarProject HMP4040controller
OPCUA server based on Quasar to control a power supply Rohde&Schwarz HMP4040 and a WinCC OA project acting as client to control it.
This project is a smaller version of [labsetupdcs](https://gitlab.cern.ch/atlas-itk-italy/labsetupdcs), built to control lab equipment (power supplies, climatic chambers, chillers, environmental sensors).

This project was built on a CC7 machine.

Following versions are used:
- Quasar v1.4.2
- open62541-compat version v1.3.6
- Cacophony [version June 2020](https://github.com/quasar-team/Cacophony/tree/3f8c4a2dbc6b0ef87e8e09a57000d0bccee2f34a)
- a version of Boost > 1.66 is necessary.

### Installing the server
<!---
This server uses open62541 (https://open62541.org/) and open62541-compat (https://github.com/quasar-team/open62541-compat). The instructions for their installation are available in the given links.
In addition the same requirements of the Quasar server listed [here](https://github.com/quasar-team/quasar/blob/master/Documentation/quasar.html). Extra requirements are:

- A version of Boost > 1.66 are necessary. If not installed in a standard location, the environment variable BOOST_ROOT must be defined, pointing to your custom installation root.
--->
To install this server, first clone this repo:

```
git clone ssh://git@gitlab.cern.ch:7999/itk-felix-sw/hmp4040controller.git
```

<!---
Then set quasar in order to use open62541 and open62541-compat:
```
cd hmp4040controller
./quasar.py set_build_config config.cmake
./quasar.py build
```
--->
The configuration of the server is in file `build/bin/config.xml`, any change according to a specific setup must be applied there: adding/removing devices, specifying config entries that must be provided by the user, define node names. A config entries that must be edited is the USB port: insert in `connectionString` the address of the USB port you are using.

<!---
The "devices" currently available that can be specified in the `build/bin/config.xml` file are:
- PowerSupplyHMP4040 (developed with Rohde&Schwarz HMP4040)
--->
More information on how to edit the config file are available in Documentation/ConfigDocumentation.html.

Now run the server:
```
cd build/bin/
./OpcUaServer
```

A message like
```
TCP network layer listening on opc.tcp://pcatlidps19:4841/
```
will show the server address.

### Installing the WinCC OA project
To install the WinCC OA project open WinCC PA (Project Administrator). Click on the icon "Register new project" (third icon from the left) and select the "HMP4040wincc" directory. 

If you start the project you might see some errors in the Log Viewer (you can open it from the WinCC Project Administrator or Console) due to missing files. To solve them open the "JCOP framework" window in GEDI and install the content of the "fwComponenents_XXXXXXXX" directory in the repository. Then restart the project; the errors should be gone. 

Another error you should see in the Log Viewer is `OpcUaCComm in Function getEndpointInfo, Get the endpoints for discovery URL opc.tcp://pcatlidps19:4841/ was not successful (Statuscode: BadCommunicationError)`: WinCC OA is trying to connect to the wrong OPC server (with URL opc.tcp://pcatlidps19:4841/). To establish the communication with your OPC server the correct URL must be set in WinCC. In order to do so open the "SysMgm" window in GEDI, then select "Driver OPC" -> "OPC UA Client". In the server section replace the URL `opc.tcp://pcatlidps19:4841/` with the server address shown when running the OPC UA server. Restart WinCC OA: if the OPC UA server is running you should see `OpcUaCServerData in Function run, Connected to OPCUA_LABSETUP @ opc.tcp://pcatlidps19:4841/` (with the correct URL of your server) in the Log Viewer.




<!---
### Installing the WinCC OA package
Before installing the fwLabSetup component into a WinCC OA project edit file `winccoa/fwLabSetup/dplist/fwLabSetup/fwLabSetupOpcua.dpl`: at line 17 replace `opc.tcp://pcatlidps19:4841/` with your server address.

In order to import the necessary DPTs and DPs to  a WinCC OA project, use Cacophony to produce the scripts for WinCC OA:
```
python Cacophony/generateStuff.py --dpt_prefix fwLabSetup --server_name OPCUA_LABSETUP --driver_number 10 --subscription OPCUA_LABSETUP_DefaultSubscription
```

Once you have WinCC OA 3.16 installed and you have created the project, copy the scripts into it:
```
cp Cacophony/generated/*ctl <winccoa-project-path>/scripts/libs/
```

Now make sure that the JCOP Installation Tool is installed in your WinCC OA project (available [here](https://jcop.web.cern.ch/jcop-framework-component-installation-tool)). Use the installation tool to install the following JCOP components of the framework (full framework available [here](https://jcop.web.cern.ch/jcop-framework-0)):
- fwCore
- fwXML
- fwTrending

**Use JCOP framework version 8.4.1** or lower, as in version 8.4.2 an incompatibilty with the new version of fwTrending was found.

Restart the project when asked.

Now you can install the fwLabSetup component (located in this repository under `winccoa/fwLabSetup/`) in the same way using the JCOP installation tool. When asked whether to activate driver or activate SIM, choose "activate driver".

In Gedi, from the project view on the left side open and run panel `fwComponents_XXXXXXXX/Panels/fwLabSetup/fwLabSetupConfiguration.pnl`: it contains three buttons that must be clicked in the order indicated by numbers (first  button `1)` to create DPTs, then button `2)` to create DPs with the correct addresses, then optionally button `3)` to configure the reading of DPEs historical values from InfluxDB). Before clicking button `2) Create DPs` you need to write your quasar server main directory in the field `<path-to-quasar-project>`(then press enter).  When pressing button `2)` the Quasar server must be running. Use button `3)` only if the `NextGenArchiver` component is installed and with manager `NextGen Archiver` running in pmon.

The main (reference) panel to control and monitor a Rohde&Schwarz HMP4040 is `panels/fwLabSetup/fwLabSetupChannelPSHMP4040.pnl`. In order to use this panel a channel DP of DP type `fwLabSetupChannelPSHMP4040`) must be assigned to the dollar parameter `$sDpName`.

A possible way to use the reference panels is to create a new empty panel, drag-and-drop the referece panel there, then assign the dollar parameter to the desired DP when prompted.
<!---
### Useful links
- Some instructions on the installation of linux-gpib and setting up WinCC OA are available [here](https://istnazfisnucl.sharepoint.com/:o:/s/TestLab/EVeWDyIxmAVBmjOyMLsMiesBVLeEWDA08fknk_XjZ6RIjQ?e=bsQ63e) (Italian)
- An installation guide is available [here](https://docs.google.com/presentation/d/1ehoMXn4T62ooTUhAFsoZKKVFxu_vJ1XwYg0zOR-VS-Y/edit?usp=sharing) (English)
--->