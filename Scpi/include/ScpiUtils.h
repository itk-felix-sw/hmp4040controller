/*
 * ScpiUtils.h
 *
 *  Created on: Nov 11, 2020
 *      Author: ressegotti
 */

//#include <gpib/ib.h>
#include <SerialTransport.h>
#include <string>
#include <sstream>
#include <iostream>
#include <LogIt.h>

#ifndef SCPI_SCPIUTILS_H_
#define SCPI_SCPIUTILS_H_


namespace Scpi {

class ScpiUtils { 
public:
	ScpiUtils();
	virtual ~ScpiUtils();

public: 
	// to open the device
	std::string m_connString = "";
	int m_dev = -1;
	int initialize (std::string, int =1);
	std::string m_idn = "";
	std::unique_ptr<SerialTransport> m_serial;

	//device channels
	int m_maxChannel = 1;	 //number of channels in the power supply
	int m_currentChannel = 1; //channel to which commands are currenlty sent to
	int selectChannel(int =1);

	//auxiliary functions
	//use default int=-1 to send command to the entire device
	//otherwise use it to select target channel
	int sendCommand(std::string, int=-1);
	template <typename T>
	int sendCommandRetval(std::string, T&, int=-1);
	template <typename T>
	int sendCommandInval(std::string, T, int=-1);
	template <typename T, typename U>
	int sendCommandInvalRetval(std::string, T, U&, int=-1);

	bool recover();
	bool restart();

private:
	std::mutex m_mutex;
}; 


}  /* namespace Scpi */

#endif /* SCPI_SCPIUTILS_H_ */
