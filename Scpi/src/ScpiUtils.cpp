/*
 * ScpiUtils.cpp
 *
 *  Created on: Nov 11, 2020
 *      Author: ressegotti
 */

#include "ScpiUtils.h"

namespace Scpi {

ScpiUtils::ScpiUtils() {
}

ScpiUtils::~ScpiUtils() {
}
  
int ScpiUtils::initialize (std::string connString, int maxCh) {

	std::lock_guard<std::mutex> lock (m_mutex);

	m_maxChannel = maxCh;		
	m_connString = connString;
	m_serial = SerialTransport::makeSerialTransport(m_connString);

	std::string cmd("*IDN?");

	m_serial->sendAndReceive(cmd, m_idn, 2);

	//set m_dev=1 if a listening device is found, otherwise m_dev=-1
	if (m_idn != "") {
		LOG(Log::INF) << "Device " << m_idn << " initialized." ;
		m_dev=1;
	}
	else {
		LOG(Log::INF) << "Error: device initialization not valid." ;
		m_dev = -1;
	        m_idn = "EMPTY";
	}

    return m_dev;
}

bool ScpiUtils::recover() {
	bool aligned = m_serial->align();
        if (!aligned) return restart();
	else          return aligned;
}

bool ScpiUtils::restart() {
	m_serial.reset(nullptr);
	m_dev = -1;
	m_idn = "EMPTY";
	while(m_dev==-1)
	{
          LOG(Log::INF) << "Connection to " << m_connString << " closed. Will try to restart it in 2 minutes." ;
      	  std::this_thread::sleep_for(std::chrono::seconds(120));
	  initialize(m_connString, m_maxChannel);
	}
	return m_dev;
}

int ScpiUtils::selectChannel(int ch) {
//function "selectChannel" to be used inside another function calling mutex

	//use ch<0 to skip channel selection
	//(e.g. for commands to the entire device)
	if (ch<0)   return 0;

	m_currentChannel = ch;
	std::stringstream ss;
	ss << ch;
	std::string s(ss.str());
	std::string command = "INST:NSEL "+s;

	return m_serial->send(command);
}

// send commands without any input/return value
int ScpiUtils::sendCommand(std::string cmd, int ch) {
	if (m_dev<0)  return -1;
	int stat = -1;
	std::lock_guard<std::mutex> lock (m_mutex);
	ScpiUtils::selectChannel(ch);
	stat = m_serial->send(cmd);
	return stat;
}

// send commands with return value
template <typename T>
int ScpiUtils::sendCommandRetval(std::string cmd, T &retval, int ch) {
	if (m_dev<0)  return -1;
	int stat = -1;
	std::lock_guard<std::mutex> lock (m_mutex);
	ScpiUtils::selectChannel(ch);
	std::string strRetval;
	stat = m_serial->sendAndReceive(cmd, strRetval);
	
	if (stat==SERIAL_TRANSPORT_TIMEOUT) 
	{
	  if (recover())  //true if alignment successful/connection successfully restarted
             stat = m_serial->sendAndReceive(cmd, strRetval);
	  else
	     return stat;
	}

	std::stringstream ss;
	ss << strRetval;
	ss >> retval;

	return stat;
}

template <>
int ScpiUtils::sendCommandRetval<bool>(std::string cmd, bool &retval, int ch) {
	if (m_dev<0)  return -1;
	int stat = -1;
	std::lock_guard<std::mutex> lock (m_mutex);
	ScpiUtils::selectChannel(ch);
	std::string strRetval;
	stat = m_serial->sendAndReceive(cmd, strRetval);

	if (stat==SERIAL_TRANSPORT_TIMEOUT) 
	{
	  if (recover())  //true if alignment successful/connection successfully restarted
             stat = m_serial->sendAndReceive(cmd, strRetval);
	  else
	     return stat;
	}

	if (strRetval.length() > 0 )
		{
		if 	(strRetval.at(0)=='1')	retval = 1;
		else if	(strRetval.at(0)=='0')	retval = 0;
		}

	return stat;
}

template <>
int ScpiUtils::sendCommandRetval<std::string>(std::string cmd, std::string &retval, int ch) {
	if (m_dev<0)  return -1;
	int stat = -1;
	std::lock_guard<std::mutex> lock (m_mutex);
	ScpiUtils::selectChannel(ch);
	stat = m_serial->sendAndReceive(cmd, retval);
	
	//can't use template version with std::stringstream when T is std::string
	//because it truncates the std::string at the first blank space character.
	//e.g.: "+0,No error" would become "+0,No"

	if (stat==SERIAL_TRANSPORT_TIMEOUT) 
	{
	  if (recover())  //true if alignment successful/connection successfully restarted
             stat = m_serial->sendAndReceive(cmd, retval);
	  else
	     return stat;
	}

	return stat;
}

// send commands with input value
template <typename T>
int ScpiUtils::sendCommandInval(std::string cmd, T inval, int ch) {
	if (m_dev<0)  return -1;
	int stat = -1;
	std::lock_guard<std::mutex> lock (m_mutex);
	ScpiUtils::selectChannel(ch);
	std::ostringstream ss;
	ss << inval;
	std::string s(ss.str());
	std::string command = cmd+" "+s;

	stat = m_serial->send(command);

	return stat;
}

template <>
int ScpiUtils::sendCommandInval<bool>(std::string cmd, bool inval, int ch) {
	if (m_dev<0)  return -1;
	int stat = -1;
	std::lock_guard<std::mutex> lock (m_mutex);
	ScpiUtils::selectChannel(ch);
	std::string s="OFF";
	if (inval)	s="ON";
	std::string command = cmd+" "+s;

	stat = m_serial->send(command);

	return stat;
}

// send commands with input and return value
template <typename T, typename U>
int ScpiUtils::sendCommandInvalRetval(std::string cmd, T inval, U &retval, int ch) {
	if (m_dev<0)  return -1;
	int stat = -1;
	std::ostringstream ss;
	ss << inval;
	std::string s(ss.str());
	std::string command = cmd+" "+s;
	std::string strRetval;
	stat = ScpiUtils::sendCommandRetval(command, retval, ch);

	return stat;
}

template int ScpiUtils::sendCommandRetval<float>(std::string cmd, float &retval, int ch);
template int ScpiUtils::sendCommandRetval<bool>(std::string cmd, bool &retval, int ch);
template int ScpiUtils::sendCommandRetval<std::string>(std::string cmd, std::string &retval, int ch);
template int ScpiUtils::sendCommandRetval<int>(std::string cmd, int &retval, int ch);

template int ScpiUtils::sendCommandInval<float>(std::string cmd, float inval, int ch);
template int ScpiUtils::sendCommandInval<bool>(std::string cmd, bool inval, int ch);
template int ScpiUtils::sendCommandInval<std::string>(std::string cmd, std::string inval, int ch);
template int ScpiUtils::sendCommandInval<int>(std::string cmd, int inval, int ch);

template int ScpiUtils::sendCommandInvalRetval<int,bool>(std::string cmd, int inval, bool &retval, int ch);

}  /* namespace Scpi */
