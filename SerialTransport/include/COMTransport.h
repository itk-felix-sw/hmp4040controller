// Serial communication via COM port
//
//
//#define BOOST_ASIO_ENABLE_HANDLER_TRACKING 1 //SET DEBUG OPTION
#include <boost/asio.hpp>
#include <boost/asio/serial_port.hpp>
#include <boost/system/error_code.hpp>
#include <boost/system/system_error.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

typedef boost::shared_ptr<boost::asio::serial_port> serial_port_ptr;


class COMTransport : public SerialTransport {
  protected:
    boost::asio::io_service io_service_;
    serial_port_ptr port_;
    boost::mutex mutex_;


  private:
     void read_handler(boost::system::error_code ec);
     std::string async_prefix;
     int conntimeout = 2000;

     std::queue<std::string> receive_fifo;
     std::queue<std::string> receive_async_fifo;
     boost::asio::streambuf receive_buf;
     boost::system::error_code read_error;
     boost::system::error_code read_async_error;
     boost::system::error_code write_error;

     std::mutex mutex_cv;
     std::condition_variable read_cv;
     std::condition_variable read_async_cv;

     void clean_fifo();

  public:
    COMTransport(const std::string& connection);
    ~COMTransport();
	virtual bool start(const char *com_port_name, int baud_rate=9600);
	virtual void stop();

    std::recursive_mutex mutex;
	
public:
    std::string address;
    // Initialize connection
    int initialize();
    // Sends a mesage
    int send(const std::string& message);
    // Receive a message
    int receive(std::string& reply, int timeout = 2000);
    // Sends a message and wait for a reply
    int sendAndReceive(const std::string& message, std::string& reply, int timeout = 2000);
    // Receive asyncronous messages
    int receiveAsync(std::string& message, int timeout = 2000);
    std::thread* asynctr;

};
