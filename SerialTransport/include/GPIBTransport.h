// Serial communication via GPIB
//

#include <gpib/ib.h>

class GPIBTransport : public SerialTransport {
  public:
    GPIBTransport(const std::string& connection);
    ~GPIBTransport();
    // Sends a mesage
    int send(const std::string& message);
    // Receive a message
    int receive(std::string& reply, int timeout = 2000);
    // Sends a message and wait for a reply
    int sendAndReceive(const std::string& message, std::string& reply, int timeout = 2000);
    // Receive asyncronous messages
    int receiveAsync(std::string& message, int timeout = 2000);
    // Initialize board connection
    int initialize();

  private:
      std::recursive_mutex mutex;
      int board;
      int pad;
      int sad;
      int tout;
      int dev;
};
