#ifndef SERIAL_TRANSPORT_INCLUDE
#define SERIAL_TRANSPORT_INCLUDE

#include <string>
#include <memory>
#include <thread>
#include <iostream>
#include <queue>
#include <condition_variable>
#include <chrono>
#include <LogIt.h>

#define SERIAL_TRANSPORT_TIMEOUT -1

class SerialTransport {
  public:
    SerialTransport(const std::string& connection) {};
    virtual ~SerialTransport() {};
    // Sends a mesage
    virtual int send(const std::string& message) = 0;
    // Receive a message
    virtual int receive(std::string& reply, int timeout = 2000) = 0;
    // Sends a message and wait for a reply
    virtual int sendAndReceive(const std::string& message, std::string& reply, int timeout = 2000) = 0;
    // Receive asyncronous messages
    virtual int receiveAsync(std::string& message, int timeout = 2000) = 0;
    // Factory
    static std::unique_ptr<SerialTransport> makeSerialTransport(const std::string& connection);
    // Re-align replies with queries
    virtual bool align() {return 1;};
  protected:
    // String manipulation
    std::string element(const std::string& line, int nel, const std::string& delimiter = "::"); 
};

//#include <TCPTransport.h>
#include <COMTransport.h>
//#include <MQTTTransport.h>
//#include <GPIBTransport.h>

#endif // !SERIAL_TRANSPORT_INCLUDE

