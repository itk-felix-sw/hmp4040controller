// Serial communication via COM port
//

#include <SerialTransport.h>

//#include <stdio.h>      // standard input / output functions
//#include <stdlib.h>
#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>    // POSIX terminal control definitions
//#include <iostream>

using namespace std;

COMTransport::COMTransport(const std::string& connection) : SerialTransport(connection) {
	address = element(connection, 1).c_str();
  	async_prefix = element(connection, 2);
  	if (element(connection, 3)!="")  conntimeout = stoi(element(connection, 3));
	initialize();
}

COMTransport::~COMTransport() {
	this->stop();
  	asynctr->join();
}


bool COMTransport::start(const char *com_port_name, int baud_rate)
{
	boost::system::error_code ec;

	if (port_) {
		std::cout << "error : port is already opened..." << std::endl;
		return false;
	}

	port_ = serial_port_ptr(new boost::asio::serial_port(io_service_));
	port_->open(com_port_name, ec);
	if (ec) {
		std::cout << "error : port_->open() failed...com_port_name="
			<< com_port_name << ", e=" << ec.message().c_str() << std::endl; 
		return false;
	}

	// option settings...
	port_->set_option(boost::asio::serial_port_base::baud_rate(baud_rate));
	port_->set_option(boost::asio::serial_port_base::character_size(8));
	port_->set_option(boost::asio::serial_port_base::stop_bits(boost::asio::serial_port_base::stop_bits::one));
	port_->set_option(boost::asio::serial_port_base::parity(boost::asio::serial_port_base::parity::none));
	port_->set_option(boost::asio::serial_port_base::flow_control(boost::asio::serial_port_base::flow_control::none));

	boost::thread g(boost::bind(&boost::asio::io_service::run, &io_service_));

  	// Post the first read operation
  	async_read_until(*port_, receive_buf, "\n", [this](boost::system::error_code ec, std::size_t) {
        if (!ec) {
          read_handler(ec);
        } else {
          port_->close();
        }
      });
  LOG(Log::INF) << "COM: Readasync started" ;
	return true;
}

void COMTransport::read_handler(boost::system::error_code ec) {
  std::unique_lock<std::mutex> guard_cv(mutex_cv);
  
  // saving response from server
  const char* data = boost::asio::buffer_cast<const char*>(receive_buf.data());
  //const char* data = receive_buf.data();
  std::istream is(&receive_buf); 
  std::string ret;
  std::getline(is, ret);
  if (ret[ret.size() - 1] == char(0xa)) ret = ret.substr(0, ret.size() - 1);
  // std::cout << "TCP<-- " << ret << std::endl;
  bool async = false;
  if (async_prefix != "") {
      if (ret.substr(0, async_prefix.size()) == async_prefix) {
          async = true;
      }
  }
  if (async) {
      receive_async_fifo.push(std::move(ret));
      read_async_error = ec;
      read_async_cv.notify_all();
  } else {
      receive_fifo.push(std::move(ret));
      read_error = ec;
      read_cv.notify_all();
  }
  receive_buf.consume(receive_buf.size());

  // submit a new request
  async_read_until(*port_, receive_buf, "\n", [this](boost::system::error_code ec, std::size_t) {
      if (!ec) {
          read_handler(ec);
      } else {
          read_error = ec;
          port_->close();
      }
  });

}

void COMTransport::stop()
{
	boost::mutex::scoped_lock look(mutex_);

	if (port_) {
		port_->cancel();
		port_->close();
		port_.reset();
	}
	io_service_.stop();
	io_service_.reset();
}


int COMTransport::initialize() {
	bool rv;
	//COMTransport serial;
	rv = this->start(address.c_str(), 115200);	
	if (!rv) {
	  return -1;
	}

	// initialize
	/*
	this->end_of_line_char(0x0d);
	this->write_some("BI010\r\n");
	this->write_some("PE011\r\n");
	this->write_some("RA01000\r\n");
	*/
	return 0;
}

int COMTransport::send(const std::string& message) { 

	std::string buf = message + "\n";
        std::lock_guard<std::recursive_mutex> guard(mutex);

	if (!port_) return -1;
	if (buf.size() == 0) return 0;

	boost::asio::write(*port_, boost::asio::buffer(buf.c_str(),buf.size()), write_error) ;

    	if (!write_error) return write_error.value();
	return 0;

}

int COMTransport::receive(std::string& reply, int timeout) {
    timeout = conntimeout;
    if (read_error) {
        reply = "";
        return read_error.value();
    }
    std::lock_guard<std::recursive_mutex> guard(mutex);
    std::unique_lock<std::mutex> guard_cv(mutex_cv);
    if (timeout == 0) {
        read_cv.wait(guard_cv, [this] {return receive_fifo.empty() == false; });
    } else {
        read_cv.wait_for(guard_cv, std::chrono::milliseconds(timeout), [this] {return receive_fifo.empty() == false; });
    }
    if (receive_fifo.empty()) {
        reply = "";
        return SERIAL_TRANSPORT_TIMEOUT;
    } else {
        reply = receive_fifo.front();
        receive_fifo.pop();
        return 0;
    }

}

int COMTransport::sendAndReceive(const std::string& message, std::string& reply, int timeout) {
    timeout = conntimeout;
    // send message
    std::lock_guard<std::recursive_mutex> guard(mutex);
    int ret = send(message);
    if (ret != 0) return ret;
    // wait until a reply is received
    ret = receive(reply, timeout);
    return ret;
}

int COMTransport::receiveAsync(std::string& message, int timeout) {
    timeout = conntimeout;
    if (async_prefix == "") {
        message = "";
        return -1;
    }
    if (read_async_error) {
        message = "";
        return read_async_error.value();
    }
    std::lock_guard<std::recursive_mutex> guard(mutex);
    std::unique_lock<std::mutex> guard_cv(mutex_cv);
    if (timeout == 0) {
        read_async_cv.wait(guard_cv, [this] {return receive_async_fifo.empty() == false; });
    }
    else {
        read_async_cv.wait_for(guard_cv, std::chrono::milliseconds(timeout), [this] {return receive_async_fifo.empty() == false; });
    }
    if (receive_async_fifo.empty()) {
        message = "";
        return SERIAL_TRANSPORT_TIMEOUT;
    } else {
        message = receive_async_fifo.front();
        receive_async_fifo.pop();
        return 0;
    }
}


void COMTransport::clean_fifo() {
  //std::unique_lock<std::mutex> guard_cv(mutex_cv);

  while (!receive_fifo.empty()) {
    receive_fifo.pop();
  }

  while (!receive_async_fifo.empty()) {
    receive_async_fifo.pop();
  }
}

