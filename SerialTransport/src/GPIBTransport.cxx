// Serial communication via GPIB
//

#include <SerialTransport.h>

GPIBTransport::GPIBTransport(const std::string& connection) : SerialTransport(connection) {
	board = std::atoi(element(connection, 1).c_str());
	pad = std::atoi(element(connection, 2).c_str());
	sad = std::atoi(element(connection, 3).c_str());
	tout = std::atoi(element(connection, 4).c_str());
	dev = -1;
	initialize();
}

GPIBTransport::~GPIBTransport() {
}

int GPIBTransport::initialize() {
	std::lock_guard<std::recursive_mutex> guard(mutex);
	int send_eoi = 0;
	int eos = 0x140a;
	int tim;
	if (tout == 0) tim = TNONE;
	else if (tout <= 1000) tim = T1s;
	else if (tout <= 3000) tim = T3s;
	else if (tout <= 10000) tim = T10s;
	else if (tout <= 30000) tim = T30s;
	else if (tout <= 100000) tim = T100s;
	else if (tout <= 300000) tim = T300s;
	else tim = T1000s;
	dev = ibdev(board, pad, sad, tim, send_eoi, eos);
	std::cout << "GPIB: initialize device " << board << "/" << pad << "/" << sad;
	if (dev > 0) {
		std::cout << " OK" << std::endl;
		return 0;
	} else {
		std::cout << " FAILED" << std::endl;
		return -1;
	}
}

int GPIBTransport::send(const std::string& message) {
	if (dev < 0) {
		initialize();
		if (dev < 0) return dev;
	}
	std::lock_guard<std::recursive_mutex> guard(mutex);
	std::string mess_c = message + char(0xa);
	int stat = ibwrt(dev, (void*)mess_c.c_str(), mess_c.size());
	return stat;
}

int GPIBTransport::receive(std::string &reply, int timeout) {
	char buf[1000];
	int len = 1000;
	int tim;
	std::lock_guard<std::recursive_mutex> guard(mutex);
	if (timeout != tout) {
		tout = timeout;
		if (tout == 0) tim = TNONE;
		else if (tout <= 1000) tim = T1s;
		else if (tout <= 3000) tim = T3s;
		else if (tout <= 10000) tim = T10s;
		else if (tout <= 30000) tim = T30s;
		else if (tout <= 100000) tim = T100s;
		else if (tout <= 300000) tim = T300s;
		else tim = T1000s;
		ibtmo(dev, tim);
	}
	int stat = ibrd(dev, buf, len);
	//std::cout << "GPIB:receive " << stat << " " << ibcnt << " " << buf << std::endl;
	if (stat & 0xC000) {
		reply = "";
		return stat;
	}
	if (buf[ibcnt - 1] == char(0xa)) {
		buf[ibcnt - 1] = 0;
	} else {
		buf[ibcnt] = 0;
	}
	reply = buf;
	return stat;
}

int GPIBTransport::sendAndReceive(const std::string& message, std::string& reply, int timeout) {
	std::lock_guard<std::recursive_mutex> guard(mutex);
	int retval = send(message);
	if (retval & 0x8000) return retval;
	return receive(reply, timeout);
}

int GPIBTransport::receiveAsync(std::string& message, int timeout) {
	std::lock_guard<std::recursive_mutex> guard(mutex);
	message = "";
	return -1;
}

