#include <SerialTransport.h>

std::unique_ptr<SerialTransport> SerialTransport::makeSerialTransport(const std::string& connection) {
//  if (connection.substr(0,3)  == "TCP") {

  /*
  if (connection.substr(0,3)  == "COM") {
    std::cout << "enter if" << std::endl;
    //return std::make_unique<TCPTransport>(connection);
    return std::make_unique<COMTransport>(connection);  // <--- Problem here?
  */
  if (connection.substr(0,3)  == "COM") {
    return std::make_unique<COMTransport>(connection);
//  } else if (connection.substr(0,3)  == "COM") {
//    return std::make_unique<COMTransport>(connection);
//  } else if (connection.substr(0,4)  == "GPIB") {
//    return std::make_unique<GPIBTransport>(connection);
//  } else if (connection.substr(0,4)  == "MQTT") {
//    return std::make_unique<MQTTTransport>(connection);
  }
  std::cout << "makeSerialTransport end" << std::endl;
  return nullptr;
}

std::string SerialTransport::element(const std::string& line, int nel, const std::string& delimiter) {
  size_t pos = 0;
  size_t fpos = 0;
  int ip = 0;

  while ((fpos = line.find(delimiter, pos)) != std::string::npos) {
     if (ip++ == nel) return line.substr(pos, fpos - pos);
     pos = fpos + delimiter.size();
  }
  if (nel == ip) return line.substr(pos);
  return "";
}

