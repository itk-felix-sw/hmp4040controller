/* © Copyright CERN, Universidad de Oviedo, 2015.  All rights not expressly granted are reserved.
 * QuasarServer.cpp
 *
 *  Created on: Nov 6, 2015
 * 		Author: Damian Abalo Miron <damian.abalo@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <thread>

#include "QuasarServer.h"
#include <LogIt.h>
#include <shutdown.h>
#include <DRoot.h>
/*
#include <DPowerSupply.h>
#include <DChannelPS.h>
#include <DActualPS.h>
*/
#include <DPowerSupplyHMP4040.h>
#include <DChannelPSHMP4040.h>
#include <DSettingsPSHMP4040.h>

//#include <DInfluxDB.h>

QuasarServer::QuasarServer() : BaseQuasarServer()
{

}

QuasarServer::~QuasarServer()
{
 
}

void QuasarServer::mainLoop()
{
    printServerMsg("Press "+std::string(SHUTDOWN_SEQUENCE)+" to shutdown server");

    // Wait for user command to terminate the server thread.
    while(ShutDownFlag() == 0)
    {
        for (Device::DPowerSupplyHMP4040 * ps : Device::DRoot::getInstance()->powersupplyhmp4040s())
   	        {
		ps->update();
		}
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    printServerMsg(" Shutting down server");

	//terminate all threads
	exitSignal.set_value();
	for (auto it = updateThreads.begin(); it != updateThreads.end(); ++it)
		(*it).join();
}

void QuasarServer::initialize()
{
    LOG(Log::INF) << "Initializing Quasar server.";

    for (Device::DPowerSupplyHMP4040 * powersupply : Device::DRoot::getInstance()->powersupplyhmp4040s())
        {
    	powersupply->initializeAddressSpaceVars();
	for (Device::DChannelPSHMP4040 * chan : powersupply->channelpshmp4040s())
	    {
	    for (Device::DSettingsPSHMP4040 * set : chan->settingspshmp4040s())
		set->initialize();
	    for (Device::DActualPSHMP4040 * act : chan->actualpshmp4040s())
		act->initialize();
	    }
        }
}

void QuasarServer::shutdown()
{
	LOG(Log::INF) << "Shutting down Quasar server.";
}

void QuasarServer::initializeLogIt()
{
	BaseQuasarServer::initializeLogIt();
    LOG(Log::INF) << "Logging initialized.";
}
