var group__fwPeripheryAddress =
[
    [ "fwPeriphAddress_get", "group__fwPeripheryAddress.html#ga27926036a53b8c11175ae8311cb1dc95", null ],
    [ "fwPeriphAddress_getMany", "group__fwPeripheryAddress.html#gaa3460bf1ac1eb5ba817f1c4c5ab19b38", null ],
    [ "fwPeriphAddress_set", "group__fwPeripheryAddress.html#ga1c2404c12f9da5cf8737f844e9453df0", null ],
    [ "fwPeriphAddress_setMany", "group__fwPeripheryAddress.html#ga7f1597b38c82b707537fb3ca9b3dbf21", null ]
];