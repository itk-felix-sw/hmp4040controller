/**@file Deprecated prototype for use of exceptions.

   @par Modification Date
        02/07/15 - Axel Voitier
 */

#uses "fwGeneral/fwGeneral.ctl"

/* TODO:
 * - test fwException_throwLastError
 * - document tests for getStackTrace, getText, rethrow, throw, match and getClass
 * - logging thrown exception onto a DP, with hierarchy filter
 * - logging in log file?
 * - reuse original fwException configuration
 * - investigate possible compatibility with old system
 */

public global time _fwException_last_read_catalog = (time)0;  // Need to be public to enable unit tests...
private global mapping _fwException_catalog_content;
private global int _fwException_last_error_code = 0;

// These special exceptions must exists in base fwException.cat.
const errClass BASE_EXCEPTION = makeError("fwException", PRIO_SEVERE, ERR_CONTROL, 1, "EX"); // Bootstrap!
const errClass BUILTIN_EXCEPTION = fwException_declare("EX.WCCOA");
const errClass FWEXCEPTION_SETUP_EXCEPTION = fwException_declare("FWEXCEPTION.SETUP");

// Some built-in exceptions.
// TODO: Complement, maybe in a separate file are there can be hundreds of them. Maybe also use a script to generate them.
const errClass INDEX_OUT_OF_RANGE_EXCEPTION = fwException_declare("-79", BUILTIN_EXCEPTION);
const errClass DIVISION_BY_ZERO_EXCEPTION = fwException_declare("-80", BUILTIN_EXCEPTION);

public errClass fwException_declare(string name, errClass parent = BASE_EXCEPTION)
{
	FWDEPRECATED();
	if (parent == BUILTIN_EXCEPTION) {
		// Create an error class that resemble a built-in one.
		// Format of name for built-in exceptions should be "catalog-code".
		// Eg. "s7-39" is error 39 from built-in catalog s7.cat stating "Invalid plc: $1".
		// Eg. "-80" is error 80 from built-in catalog _errors.cat (special default catalog) stating "Division by zero".
		dyn_string name_split = strsplit(name, "-");
		string catalog = name_split[1];
		int code = (int)name_split[2];
		return makeError(catalog, PRIO_SEVERE, ERR_CONTROL, code);

	} else if (getErrorCatalog(parent) == "fwException") {
		// Create a custom error class whose parent is given (default is "EX" BASE_EXCEPTION)
		_fwException_readCatalog();

		string ex_name;
		if (strpos(name, fwException_getClass(parent)) == 0) {
			ex_name = name;
		} else {
			ex_name = fwException_getClass(parent) + "." + name;
		}

		if (mappingHasKey(_fwException_catalog_content, ex_name)) { // If already exists, use its error code.
			return makeError("fwException", PRIO_SEVERE, ERR_CONTROL, _fwException_catalog_content[ex_name]);
		} else { // Otherwise ask for adding it to the catalog file.
			_fwException_addErrorToCatalog(_fwException_last_error_code + 1, ex_name);
			return makeError("fwException", PRIO_SEVERE, ERR_CONTROL, _fwException_catalog_content[ex_name]);
		}


	} else {
		// Create a custom error class whose parent is a built-in (or any non-fwException) exception.
		string ex_name = "WCCOA." + fwException_getClass(parent) + "." + name;
		return fwException_declare(ex_name);
	}
}

public void fwException_throw(errClass exceptionClass, string exceptionNote = "")
{
	FWDEPRECATED();
	throw(_fwException_make(exceptionClass, exceptionNote));
}

public void fwException_rethrow()
{
	FWDEPRECATED();
	errClass ex = fwException_get();

	throw(_fwException_makeFrom(ex));
}

public bool fwException_match(string patternClassMatch, errClass ex = fwException_get())
{
	FWDEPRECATED();
	dyn_bool matchs = patternMatch(patternClassMatch, makeDynString(fwException_getClass(ex), fwException_getText()));
	bool match;

	for (int i = 1; i <= dynlen(matchs); i++)
		match |= matchs[i];
	return match;
}

public errClass fwException_get()
{
	FWDEPRECATED();
	return getLastException()[1];
}

public string fwException_getClass(errClass ex = fwException_get())
{
	FWDEPRECATED();
	if (getErrorCatalog(ex) == "fwException") {
		return strsplit(getErrorText(ex), ",")[1];
	} else {
		return getErrorCatalog(ex) + "-" + getErrorCode(ex);
	}
}

public string fwException_getText(errClass ex = fwException_get())
{
	FWDEPRECATED();
	if (getErrorCatalog(ex) == "fwException") {
		dyn_string notes = strsplit(getErrorText(ex), ",");
		if (dynlen(notes) > 1) {
			return strltrim(notes[2]);
		} else {
			return "";
		}
	} else {
		return getErrorText(ex);
	}
}

public dyn_string fwException_getStackTrace(errClass ex = fwException_get())
{
	FWDEPRECATED();
	int pos = strpos(getErrorText(ex), "Original stack trace:\n");

	if (pos >= 0) {
		string stackTrace = substr(getErrorText(ex), pos);
		strreplace(stackTrace, " | ", "|");
		strreplace(stackTrace, "Original stack trace:\n", "");
		return strsplit(stackTrace, "|");
	} else {
		return getErrorStackTrace(ex);
	}
}

private errClass _fwException_make(errClass exceptionClass, string exceptionNote)
{
	FWDEPRECATED();
	if (exceptionNote != "") {
		return makeError(
			getErrorCatalog(exceptionClass),
			getErrorPriority(exceptionClass),
			getErrorType(exceptionClass),
			getErrorCode(exceptionClass),
			exceptionNote);
	} else {
		return makeError(
			getErrorCatalog(exceptionClass),
			getErrorPriority(exceptionClass),
			getErrorType(exceptionClass),
			getErrorCode(exceptionClass));
	}
}

private errClass _fwException_makeFrom(errClass originalEx)
{
	FWDEPRECATED();
	if (getErrorCatalog(originalEx) == "fwException") {
		return makeError(
			getErrorCatalog(originalEx),
			getErrorPriority(originalEx),
			getErrorType(originalEx),
			getErrorCode(originalEx),
			fwException_getText(originalEx),
			"Original stack trace:\n" + getErrorStackTrace(originalEx));
	} else {
		dyn_string notes = strsplit(getErrorText(originalEx), ",");
		if (dynlen(notes) > 1) {
			string note = getErrorText(originalEx);
			strreplace(note, notes[1] + ", ", "");
			return makeError(
				getErrorCatalog(originalEx),
				getErrorPriority(originalEx),
				getErrorType(originalEx),
				getErrorCode(originalEx),
				note,
				"Original stack trace:\n" + getErrorStackTrace(originalEx));
		} else {
			return makeError(
				getErrorCatalog(originalEx),
				getErrorPriority(originalEx),
				getErrorType(originalEx),
				getErrorCode(originalEx),
				"Original stack trace:\n" + getErrorStackTrace(originalEx));
		}
	}
}

private void _fwException_readCatalog()
{
	FWDEPRECATED();
	string catalog_path = getPath(MSG_REL_PATH, "fwException.cat");

	if (_fwException_last_read_catalog <= getFileModificationTime(catalog_path)) {
		_fwException_last_read_catalog = getFileModificationTime(catalog_path);
		string catalog;
		fileToString(catalog_path, catalog);

		mappingClear(_fwException_catalog_content);
		int errCode;
		dyn_string catalog_lines = strsplit(catalog, "\n");
		int arrayLength = dynlen(catalog_lines);
		for (int i = 1; i <= arrayLength; i++) {
			int sep = strpos(catalog_lines[i], ",");
			errCode = (int)substr(catalog_lines[i], 0, sep);
			string errText = substr(catalog_lines[i], sep + 1);
			_fwException_catalog_content[errText] = errCode;
		}
		_fwException_last_error_code = errCode;
	}
}

private void _fwException_addErrorToCatalog(int error_code, string error_name)
{
	FWDEPRECATED();
	string catalog_path = getPath(MSG_REL_PATH, "fwException.cat", 0, 1);   // Get catalog from main project path

	if (catalog_path == "") {                                               // If does not exists in main project path, copy the one from the component
		catalog_path = getPath(MSG_REL_PATH, "fwException.cat");
		string target_path = getPath(MSG_REL_PATH, "", 0, 1) + "fwException.cat";
		bool err = copyFile(catalog_path, target_path);
		if (!err)
			fwException_throw(FWEXCEPTION_SETUP_EXCEPTION, "Could not copy catalog file from " + catalog_path + " to " + target_path);

		catalog_path = getPath(MSG_REL_PATH, "fwException.cat");         // Should be the local one now
	}

	file catalog_file = fopen(catalog_path, "a");
	int err = ferror(catalog_file);
	if (err != 0)
		fwException_throw(FWEXCEPTION_SETUP_EXCEPTION, "Could not open catalog file " + catalog_path + ". Err=" + err);

	fprintf(catalog_file, "%05d,%s\n", error_code, error_name);     // Return line MUST be at the end otherwise WCCOA cannot find the error text for the last line in the file...
	fclose(catalog_file);

	_fwException_readCatalog();
}
