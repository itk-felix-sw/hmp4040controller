/**@file

   Set of functions for handling exceptions

   @par Creation Date
        4/12/00

   @par PVSS managers
        VISION

   @author
        JCOP Framework Team
 */

//@{
/** displays any exceptions in the exceptionDisplay panel.

   @par Usage
        Public

   @par PVSS managers
        VISION

   @param exceptionInfo details of any exceptions
 */
void fwExceptionHandling_display(dyn_string &exceptionInfo)
{
	if (dynlen(exceptionInfo) <= 0) return;

	if (!globalExists("g_fwExceptionHandlingDisplay")) _fwExceptionHandling_initialise();
	if (!g_fwExceptionHandlingDisplay) return;

	// there seems to be a race condition if the function is called many times, which
	// causes modal dialog boxes to be popped up in parallel that leads to UI lockups
	// (ETM-1607).
	// Therefore we make a blocking dialog-box, and only a single one

	dyn_float df;
	dyn_string ds;
	ChildPanelOnCentralModalReturn("fwGeneral/fwExceptionDisplay.pnl",
								   "Exception Details",
								   makeDynString("$asExceptionInfo:" + exceptionInfo),
								   df, ds
								   );
	dynClear(exceptionInfo);
}

/** Displays any exceptions in the exception log list.

   @par Usage
        Public

   @par PVSS managers
        VISION

   @param exceptionInfo details of any exceptions
   @param listName selection list used to display log messages
 */
void fwExceptionHandling_log(dyn_string &exceptionInfo, string listName)
{
	int i;
	string exceptionDetails;

	if (dynlen(exceptionInfo) <= 0)
		return;

	for (i = 1; i <= (dynlen(exceptionInfo)); i += 3) {
		exceptionDetails = exceptionInfo[i] + ": " + exceptionInfo[i + 1] + ", Code: " + exceptionInfo[i + 2];
		setValue(listName, "appendItem", exceptionDetails);
	}
	exceptionInfo = makeDynString();
}

void _fwExceptionHandling_initialise()
{
	bool inhibitDisplayExceptions;

	addGlobal("g_fwExceptionHandlingDisplay", BOOL_VAR);

	if (dpExists(fwException_SETTINGS_DP)) {
		dpGet(fwException_SETTINGS_DP + ".inhibitDisplayWindow", inhibitDisplayExceptions);
	} else {
		inhibitDisplayExceptions = FALSE;
	}

	g_fwExceptionHandlingDisplay = !inhibitDisplayExceptions;
}

//@}

