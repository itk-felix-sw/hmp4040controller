// $License: NOLICENSE
//--------------------------------------------------------------------------------
/**
  @file $relPath
  @copyright $copyright
  @author ressegotti
*/

//--------------------------------------------------------------------------------
// Libraries used (#uses)

//--------------------------------------------------------------------------------
// Variables and Constants


//--------------------------------------------------------------------------------
//@public members
//--------------------------------------------------------------------------------


//--------------------------------------------------------------------------------
//@private members
//--------------------------------------------------------------------------------


//adapted from JCOP fwGeneral/fwColourTable.ctl

#uses "fwGeneral/fwColour.ctl"
#uses "fwGeneral/fwColourTable.ctl"



/**This function controls the background colour of the cell of a table from within which the function was called.
        The colour is controlled by dpConnecting to the invalid bit of the given data point element, and if it exists,
        the alert active bit and the alert colour are also connected to.
        When the function is called, and any time when the alert state or invalid state changes, a function is called
        which evaluates the current state, selects the relevant colour and sets the background colour of the graphical item.

   @par Modification History
        29/03/01 Oliver Holme   Added functionality for connecting to dpes of type STRUCTURE
                                                        This makes it possible to connect to the state of a summary alert

   @par Constraints

   @par Usage
        Public

   @par PVSS managers
        VISION

   @param exceptionInfo details of any exceptions are placed in here.
 */
void fwLabSetupColourTable_connectCellBackColToValueStatus(dyn_string &exceptionInfo)
{
	int configType, len, cellPos, i, iResType;
	string dpe;

	len = dynlen(gListOfDpElementToConnect);
//DebugN(gListOfDpElementToConnect, len);

	for (i = 1; i <= len; i++) {
    if (gListOfDpElementToConnect[i]=="")  continue;
    else if (!dpExists(gListOfDpElementToConnect[i])) {
			fwException_raise(      exceptionInfo,
									"ERROR",
									"fwColourTable_connectCellBackColToValueStatus(): The data point element\n\"" + gListOfDpElementToConnect[i] + "\n\" does not exist",
									"");
			this.cellBackColRC(i - 1, "status", "DpDoesNotExist");
//DebugN("DpDoesNotExist", gListOfDpElementToConnect[i]);
		} else {
			dpe = gListOfDpElementToConnect[i];

			iResType = dpGet(dpe + ":_alert_hdl.._type", configType);

//DebugN("res",dpe);
			switch (configType) {
			  case DPCONFIG_SUM_ALERT:                          // summary alert
				if (dpConnect(   "_fwColourTable_calculateColourWithSummaryAlertCB",
								 dpe + ":_alert_hdl.._act_state_color",
								 dpe + ":_alert_hdl.._active") == -1) {
					fwException_raise(      exceptionInfo,
											"INFO",
											"fwColourTable_connectCellBackColToValueStatus(): Connecting to the status of the data point element\n\"" + dpe + "\" was unsuccessful",
											"");
					this.cellBackColRC(i - 1, "status", "DpDoesNotExist");
				}
				break;
			  case DPCONFIG_NONE:                               // no alert
				//DebugN("no alertValue", gListOfDpElementToConnect[i]);
				if (dpElementType(dpe) == DPEL_STRUCT) {
					//show OK state if dpe has no online value (ie. dpe is of type structure)
					_fwColourTable_calculateColourWithoutAlertCB(dpe, 0);
				} else {
					if (dpConnect(   "_fwColourTable_calculateColourWithoutAlertCBValue",
									 dpe + ":_online.._invalid",
									 dpe + ":_online.._value") == -1) {
						fwException_raise(      exceptionInfo,
												"INFO",
												"fwColourTable_connectCellBackColToValueStatus(): Connecting to the status of the data point element\n\"" + dpe + "\" was unsuccessful",
												"");
						this.cellBackColRC(i - 1, "status", "DpDoesNotExist");
					}
				}
				break;
			  default:                          // any alert apart from summary
				//DebugN("alertValue", gListOfDpElementToConnect[i]);
				if (dpConnect(   "_fwColourTable_calculateColourWithAlertCBValue",
								 dpe + ":_alert_hdl.._act_state_color",
								 dpe + ":_alert_hdl.._active",
								 dpe + ":_online.._invalid",
								 dpe + ":_online.._value") == -1) {
					fwException_raise(      exceptionInfo,
											"INFO",
											"fwColourTable_connectCellBackColToValueStatus(): Connecting to the status of the data point element\n\"" + dpe + "\" was unsuccessful",
											"");
					this.cellBackColRC(i - 1, "status", "DpDoesNotExist");
				}
				break;
			}
		}
	}
}
