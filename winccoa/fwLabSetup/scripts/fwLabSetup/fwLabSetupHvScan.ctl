// $License: NOLICENSE
//--------------------------------------------------------------------------------
/**
  @file $relPath
  @copyright $copyright
  @author ressegotti
*/

//--------------------------------------------------------------------------------
// used libraries (#uses)

//--------------------------------------------------------------------------------
// variables and constants

//--------------------------------------------------------------------------------
/**
*/


main()
{
  dyn_string dpsHvScan = dpNames("*", "fwLabSetupHvScan");

  if (dynlen(dpsHvScan)>0)
  {
    for (int i=1; i<=dynlen(dpsHvScan); i++)
    {
      if(dpConnect("scanTriggeredCB", FALSE, dpsHvScan[i]+".scan.triggered") == 0)
      {
        DebugTN("INFO: HvScan manager dpConnected to "+dpsHvScan[i]);
      }
      else
      {
        DebugTN("Error: HvScan manager could not dpConnect to "+dpsHvScan[i]);
      }
    }

  }
}

int max_count=50;

bool applySettingFloat(string settingDp, string readbackDp, float val)
{
  float rbVal;
  //try 10 times and check (from the reaback) that the setting was accepted by the device
  bool ok = false;
  int count=0;
  while ( !ok && count<max_count )
    {
    int ret = dpSetWait(settingDp, val);
    dpGet(readbackDp, rbVal);
    if (fabs(val-rbVal) <= fabs(val*0.001)) ok=TRUE;  //need to use <= (not <) to work also with 0 setting
    //if (val==rbVal) ok=TRUE;
    count++;
    if(!ok) delay(0,500);  //wait 0,5 s to give time the readback to get updated
    }
  return ok;
}

bool applySettingBool(string settingDp, string readbackDp, bool val)
{
  bool rbVal;
  //try 10 times and check (from the readback) that the setting was accepted by the device
  bool ok = false;
  int count=0;
  while ( !ok && count<max_count )
    {
    int ret = dpSetWait(settingDp, val);
    dpGet(readbackDp, rbVal);
    if (val==rbVal) ok=TRUE;
    count++;
    if(!ok) delay(0,500);  //wait 0,5 s to give time the readback to get updated
    }
  return ok;
}

bool applySettingInt(string settingDp, string readbackDp, int val)
{
  int rbVal;
  //try 10 times and check (from the readback) that the setting was accepted by the device
  bool ok = false;
  int count=0;
  while ( !ok && count<max_count )
    {
    int ret = dpSetWait(settingDp, val);
    dpGet(readbackDp, rbVal);
    if (val==rbVal) ok=TRUE;
    count++;
    if(!ok) delay(0,500);  //wait 0,5 s to give time the readback to get updated
    }
  return ok;
}

void appendLog(string scanDp, string line)
{
  dyn_string logg;
  dpGet(scanDp+".scan.log", logg);
  string now = formatTime("%d/%m/%y %H:%M:%S", getCurrentTime());
  line = now+": "+line;
  dynAppend(logg, line); //append at the end
  //dynInsertAt(logg, line, 1);  //insert at the beginning
  dpSetWait(scanDp+".scan.log", logg);
}

void appendError(string scanDp, string line)
{
  dyn_string err;
  dpGet(scanDp+".scan.errors", err);
  string now = formatTime("%d/%m/%y %H:%M:%S", getCurrentTime());
  line = now+": "+line;
  dynAppend(err, line); //append at the end
  //dynInsertAt(err, line, 1);  //insert at the beginning
  dpSetWait(scanDp+".scan.errors", err);
}

bool checkTriggered(string scanDp)
{
  bool triggered;
  dpGet(scanDp+".scan.triggered", triggered);
  return triggered;
}

bool checkIsOff(string scanDp)
{
    bool isOn=true;
    string supplyDp;
    dpGet(scanDp+".supplyDatapoint", supplyDp);
    dpGet(supplyDp+"/general/readback.onOff", isOn);
    if (!isOn)
     {
     appendError(scanDp, "Source found unexpectedly off. Quitting HV scan.");
     }
  return !isOn;
}

void checkErrors(string scanDp)
{
  bool vTrip=0, iTrip=0, itkTrip=0;
  int vTripCnt=0, iTripCnt=0, itkTripCnt=0;
  string dp;
  string supplyDp;
  dpGet(scanDp+".supplyDatapoint", supplyDp);

  bool ongoing = true;

  //continue checking for errors as long as the current scan is ongoing
  while(ongoing)
  {
    //sense vTrip
    dp=supplyDp + "/sense/readback.voltageTrip";
    dpGet(dp, vTrip);
    //sense iTrip
    dp=supplyDp + "/sense/readback.currentTrip";
    dpGet(dp, iTrip);
    //general interlockTrip
    dp=supplyDp + "/general/readback.interlockTrip";
    dpGet(dp, itkTrip);

    //do "appendError" only at first occurrence
    //(not to fill the error log with tens of lines with same error)
    if(vTrip)
      {
      vTripCnt++;
      if (vTripCnt==1)
        appendError(scanDp, "Error: voltage tripped!");
      }
    if(iTrip)
      {
      iTripCnt++;
      if (iTripCnt==1)
        appendError(scanDp, "Error: current tripped!");
      }
    if(itkTrip)
      {
      itkTripCnt++;
      if (itkTripCnt==1)
        appendError(scanDp, "Error: interlock tripped!");
      }

    //reset counter to zero if trip error goes away
    if(!vTrip)
      {
      vTripCnt=0;
      }
    if(!iTrip)
      {
      iTripCnt=0;
      }
    if(!itkTrip)
      {
      itkTripCnt=0;
      }

    delay(1);
    dpGet(scanDp+".scan.ongoing", ongoing);
  }
}

void earlyExit(string scanDp)
{
  appendLog(scanDp, "Stopping HV scan. Scan not completed.");
  //SWITCH OFF? gently or simply "off"?

  //gently switch off
  string supplyDp;
  float voltage, parInitialV, parStep;
  int parDelay;
  dpGet(scanDp+".supplyDatapoint", supplyDp);
  dpGet(scanDp+".parameters.initialVoltage", parInitialV);
  dpGet(scanDp+".parameters.voltageStep", parStep);
  dpGet(supplyDp+"/source/readback.voltageLevel", voltage);  //get initial value for the ramp down
  dpGet(scanDp+".parameters.delay", parDelay);       //s

  bool isOn;
  dpGet(supplyDp+"/general/readback.onOff", isOn);
  if (!isOn)   appendLog(scanDp, "Source already off. Skipping ramp down. Scan not completed.");
  else
  {
    appendLog(scanDp, "Starting ramp down.");
    while ( voltage<parInitialV )
    {
      //int step=1;
      delay(parDelay);  //if delay is here: it waits at current voltage level (whatever it is) before starting ramp down, it doesn't wait at the last point (typically zero V)
      voltage += parStep;

      if( !applySettingFloat(supplyDp+"/source/settings.voltageLevel",
                             supplyDp+"/source/readback.voltageLevel",
                             voltage) )
      {
        appendError(scanDp, "Could not set voltage to "+voltage+" V. HV scan aborted.");
        earlyExit(scanDp); return;
      }
        appendLog(scanDp, "Ramp down: "+voltage+" V.");

      //quit ramping if source becomes off for any reason
      dpGet(supplyDp+"/general/readback.onOff", isOn);
      if (!isOn)
      {
        appendLog(scanDp, "Source found off. Quitting ramp down.");
        break;
      }

    }

    if( !applySettingBool(supplyDp+"/general/settings.onOff",
        supplyDp+"/general/readback.onOff",
        FALSE) )
      {
        appendError(scanDp, "Could not switch off the source. Check the source!");
        earlyExit(scanDp); return;
      }
    appendLog(scanDp, "Source off. Scan not completed.");
  }

  //"immediate" switch off
  /*
  string supplyDp;
  dpGet( scanDp+".supplyDatapoint", supplyDp);
  if( !applySettingBool(supplyDp+"/general/settings.onOff",
                        supplyDp+"/general/readback.onOff",
                        FALSE) )
  {
    appendError(scanDp, "Could not switch off the source. Check the source!");
  }
  else
    appendLog(scanDp, "Source switched off.");
  //end switch off
  */

  dpSetWait(scanDp+".scan.endTime",  getCurrentTime());
  dpSetWait(scanDp+".scan.ongoing",   FALSE);
  dpSetWait(scanDp+".scan.triggered", FALSE);
}//earlyExit


scanTriggeredCB(string dp, bool ongoing)
{
  string scanDp;
  string supplyDp;
  int ret;

  scanDp = dpSubStr(dp, DPSUB_SYS_DP);
  dpGet( scanDp+".supplyDatapoint", supplyDp);

  ////////////// check that no other HV scan or ramp is currently ongoing ////////////////////////
  bool rampOngoing=false;
  bool hvScanOngoing=false;

  if (dpExists(supplyDp+"HvScan"))  dpGet(supplyDp+"HvScan.scan.ongoing", hvScanOngoing);
  if (dpExists(supplyDp+"Ramp"))    dpGet(supplyDp+"Ramp.rampOngoing", rampOngoing);

  if (rampOngoing || hvScanOngoing)
  {
  		DebugTN("ERROR: Another ramp or HV scan is ongoing.\nAborting HV scan with "+supplyDp+".");
       return;
  }

  ///////// CLEAN SCAN LOG AND ERRORS /////////////////////////////////////////////
  bool triggered;
  dpGet(scanDp+".scan.triggered", triggered);
  if (!triggered) return;

  dpSetWait(scanDp+".scan.errors",     makeDynString());
  dpSetWait(scanDp+".scan.log",        makeDynString());

  if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
  appendLog(scanDp, "Starting HV scan.");

  ////////// 1) READ SCAN PARAMETERS //////////////////////////////////////////////
  float parCompliance;
  float parInitialV;
  float parStep;
  int   parN;
  int   parDelay;
  float parFinalV;
  bool  parLoopOverOuts;
  int   parNrMeasPerStep;
  bool  keepCurrentVrange;
  bool  parUseCurrentSourceIrange, parUseCurrentSourceVrange;
  bool  parUseCurrentSenseIrange, parUseCurrentSenseVrange;

  appendLog(scanDp, "Reading scan parameters.");

  dpGet(scanDp+".parameters.complianceCurrent",       parCompliance);  //uA
  dpGet(scanDp+".parameters.initialVoltage",          parInitialV);    //V, >0
  dpGet(scanDp+".parameters.voltageStep",             parStep);        //V, >0
  dpGet(scanDp+".parameters.numberOfSteps",           parN);
  dpGet(scanDp+".parameters.delay",                   parDelay);       //s
  dpGet(scanDp+".parameters.finalVoltage",            parFinalV);      //V, >0
  dpGet(scanDp+".parameters.loopOverDigitalOutputs",  parLoopOverOuts);
  dpGet(scanDp+".parameters.nrOfMeasurementsPerStep", parNrMeasPerStep);
  dpGet(scanDp+".parameters.useCurrentSourceVRange",  parUseCurrentSourceVrange);
  dpGet(scanDp+".parameters.useCurrentSourceIRange",  parUseCurrentSourceIrange);
  dpGet(scanDp+".parameters.useCurrentSenseVRange",  parUseCurrentSenseVrange);
  dpGet(scanDp+".parameters.useCurrentSenseIRange",  parUseCurrentSenseIrange);


  dpSet(scanDp+".scan.loopOverDigitalOutputs",  parLoopOverOuts);  //copy value used in this scan to .SCAN.loopOverDigitalOutputs, will be used when saving data to file


  //force initialVoltage, voltageSetp, finalVoltage, nrOfSteps, Delay, NrOfMeasPerStep to be positive
  if (parInitialV<0)
  {
    parInitialV = fabs(parInitialV);
    dpSet(scanDp+".parameters.initialVoltage", parInitialV);
  }
  if (parStep<0)
  {
    parStep=fabs(parStep);
    dpSet(scanDp+".parameters.voltageStep", parStep);
  }
  if (parFinalV<0)
  {
    parFinalV=fabs(parFinalV);
    dpSet(scanDp+".parameters.finalVoltage", parFinalV);
  }
  if (parN<0)
  {
    parN=fabs(parN);
    dpSet(scanDp+".parameters.numberOfSteps", parN);
  }
  if (parDelay<0)
  {
    parDelay=fabs(parDelay);
    dpSet(scanDp+".parameters.delay", parDelay);
  }
  if (parNrMeasPerStep<0)
  {
    parNrMeasPerStep=fabs(parNrMeasPerStep);
    dpSet(scanDp+".parameters.nrOfMeasurementsPerStep", parNrMeasPerStep);
  }

  if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

  /////// 2) WRITE SCAN DATAPOINT ELEMENTS //////////////////////////////////////
  dpSetWait(scanDp+".data.singleOut.current", makeDynFloat());
  dpSetWait(scanDp+".data.singleOut.voltage", makeDynFloat());
  dpSetWait(scanDp+".data.singleOut.currentSigma", makeDynFloat());
  dpSetWait(scanDp+".data.singleOut.voltageSigma", makeDynFloat());
  dpSetWait(scanDp+".data.singleOut.stepDuration", 0);
  dpSetWait(scanDp+".data.singleOut.time", makeDynFloat());
  dpSetWait(scanDp+".data.out1.current",      makeDynFloat());
  dpSetWait(scanDp+".data.out1.voltage",      makeDynFloat());
  dpSetWait(scanDp+".data.out1.currentSigma", makeDynFloat());
  dpSetWait(scanDp+".data.out1.voltageSigma", makeDynFloat());
  dpSetWait(scanDp+".data.out1.stepDuration", 0);
  dpSetWait(scanDp+".data.out1.time", makeDynFloat());
  dpSetWait(scanDp+".data.out2.current",      makeDynFloat());
  dpSetWait(scanDp+".data.out2.voltage",      makeDynFloat());
  dpSetWait(scanDp+".data.out2.currentSigma", makeDynFloat());
  dpSetWait(scanDp+".data.out2.voltageSigma", makeDynFloat());
  dpSetWait(scanDp+".data.out2.stepDuration", 0);
  dpSetWait(scanDp+".data.out2.time", makeDynFloat());
  dpSetWait(scanDp+".data.out3.current",      makeDynFloat());
  dpSetWait(scanDp+".data.out3.voltage",      makeDynFloat());
  dpSetWait(scanDp+".data.out3.currentSigma", makeDynFloat());
  dpSetWait(scanDp+".data.out3.voltageSigma", makeDynFloat());
  dpSetWait(scanDp+".data.out3.stepDuration", 0);
  dpSetWait(scanDp+".data.out3.time", makeDynFloat());
  dpSetWait(scanDp+".data.out4.current",      makeDynFloat());
  dpSetWait(scanDp+".data.out4.voltage",      makeDynFloat());
  dpSetWait(scanDp+".data.out4.currentSigma", makeDynFloat());
  dpSetWait(scanDp+".data.out4.voltageSigma", makeDynFloat());
  dpSetWait(scanDp+".data.out4.stepDuration", 0);
  dpSetWait(scanDp+".data.out4.time", makeDynFloat());
  dpSetWait(scanDp+".scan.ongoing",           TRUE);
  dpSetWait(scanDp+".scan.startTime",         getCurrentTime());
  dpSetWait(scanDp+".scan.endTime",        0);

  /////// start "dpConnect" on errors /////
  startThread("checkErrors", scanDp);

  /////// 3a) SEND *RST ////////////////////////////////////////////////////////

  //get current voltage/current ranges
  float senseVRange, senseIRange, sourceVRange, sourceIRange;
  if (parUseCurrentSenseVrange)
  {
    dpGet(supplyDp+"/sense/readback.voltageRangeMax", senseVRange); //sense voltage range
  }
  if (parUseCurrentSourceVrange)
  {
    dpGet(supplyDp+"/source/readback.voltageRangeMax", sourceVRange); //source voltage range
  }
  if (parUseCurrentSenseIrange)
  {
    dpGet(supplyDp+"/sense/readback.currentRangeMax", senseIRange); //sense current range
  }
  if (parUseCurrentSourceIrange)
  {
    dpGet(supplyDp+"/source/readback.currentRangeMax", sourceIRange); //source current range
  }


  if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
  dpSetWait(supplyDp+"/commands.reset", TRUE);
  appendLog(scanDp, "*RST sent.");
  delay(1);
  if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

  //setVoltageMode?

  //restore inital voltage/current ranges if necessary
  if (parUseCurrentSenseVrange)
  {
    dpSetWait(supplyDp+"/sense/settings/voltageRange.setRangeMax", senseVRange); //sense voltage range
  }
  if (parUseCurrentSourceVrange)
  {
    dpSetWait(supplyDp+"/source/settings/voltageRange.setRangeMax", sourceVRange); //source voltage range
  }
  if (parUseCurrentSenseIrange)
  {
    dpSetWait(supplyDp+"/sense/settings/currentRange.setRangeMax", senseIRange); //sense current range
  }
  if (parUseCurrentSourceIrange)
  {
    dpSetWait(supplyDp+"/source/settings/currentRange.setRangeMax", sourceIRange); //source current range
  }

  /////// 3b) SET COMPLIANCE ////////////////////////////////////////////////////
  if( !applySettingFloat(supplyDp+"/sense/settings.currentComplianceLimit",
                    supplyDp+"/sense/readback.currentComplianceLimit",
                    parCompliance*1.0e-6) )
    {
    appendError(scanDp, "Could not apply current compliance. HV scan aborted.");
    earlyExit(scanDp); return;
    }
  float currentRb;
  dpGet(supplyDp+"/sense/readback.currentComplianceLimit", currentRb);
  string i0S;
  sprintf(i0S, "%3.1f", currentRb*1.0e6);
  appendLog(scanDp, "Compliance current set to "+i0S+" uA.");

  if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

  //////// 4) RAMP ////////////////////////////////////////////////////////////////////



  dyn_int outs;
  if (parLoopOverOuts)  outs = makeDynInt(1,2,3,4);
  else                  outs = makeDynInt(0);


  for (int i=1; i<=dynlen(outs); i++)  //1 or 4 elements
    {
      ////////// Set and Loop Over Digital Output /////////////////////////////////////
      string dataDpe="singleOut";
      if (dynlen(outs)==4)
      {
      int outI = outs[i];  //current digital output
      string outS=outI;
      dataDpe = "out"+outS;
      float digOutSet = pow(2,(outI-1));
      appendLog(scanDp, "**** Setting digital output "+outI+" ****");
      if( !applySettingInt(supplyDp+"/digitalOutput/settings.output",
                           supplyDp+"/digitalOutput/actual.output",
                           digOutSet) )
        {
        appendError(scanDp, "Could not set the digital output to "+digOutSet+". HV scan aborted.");
        earlyExit(scanDp); return;
        }
      }
      if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
     //ramp(scanDp, supplyDp, parInitialV, parStep, parN, parDelay);


      ////////////////////////////////// RAMP UP //////////////////////////////////////////////////////////////////////////////////////////////////////
         ///////// Set initial Voltage /////////////////////////////
         if( !applySettingFloat(supplyDp+"/source/settings.voltageLevel",
                                supplyDp+"/source/readback.voltageLevel",
                                -1.0*parInitialV) )
           {
             appendError(scanDp, "Could not set the initial voltage to "+parInitialV+". HV scan aborted.");
             earlyExit(scanDp); return;
           }
         appendLog(scanDp, "Initial voltage -"+parInitialV+" V set.");
         if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

         /////// Switch On //////////////////////////////////
         if( !applySettingBool(supplyDp+"/general/settings.onOff",
                               supplyDp+"/general/readback.onOff",
                               TRUE) )
         {
         appendError(scanDp, "Could not switch on the source. HV scan aborted.");
         earlyExit(scanDp); return;
         }
         appendLog(scanDp, "Source switched on.");
         if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

         time timeOn;
         dpGet(supplyDp+"/general/readback.onOff" + ":_online.._stime", timeOn);


         ////////////// Take measurements at step 0 //////////////////////////
         time measStepStart, measStepStop;  //to calculate step duration
         time referenceTime; //time "zero"
         float voltage = -1.0*parInitialV;
         dyn_float currentData, voltageData;           //to contain data to be written in dps *.data.*.current/voltage
         dyn_int timeCount;                            //to contain data to be written in dps *.data.*.time
         dyn_float currentSigmaData, voltageSigmaData; //to contain data to be written in dps *.data.*.currentSigma/voltageSigma
         string iMonS, vMonS, iMonSigmaS, vMonSigmaS;

         appendLog(scanDp, "Step 0: measuring...");
         measStepStart = getCurrentTime();
         referenceTime = measStepStart;
         if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
         if (checkIsOff(scanDp)) {earlyExit(scanDp); return;}
         delay(parDelay);
         if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
         if (checkIsOff(scanDp)) {earlyExit(scanDp); return;}

         //new
         int nMeas=1;

         float stepDuration;
         dyn_float dynStepDuration;
         time  timeiMon, timevMon;
         float measiMon, measvMon;
         dyn_float dynMeasiMon, dynMeasvMon;  //sets of measurements for this voltage step, will be used to calculate average value

         //make sure first measurement accepted has timestamp > of time of switch on:
         //timeiMon, timevMon will be used in while loop below for this check
         timeiMon = makeTime(year(timeOn), month(timeOn), day(timeOn), hour(timeOn), minute(timeOn), second(timeOn)+parDelay, milliSecond(timeOn), daylightsaving(timeOn) );
         timevMon = timeiMon;

         //take repeated measurements for this voltage step,
         //verifying that each new measurement has a different timestamp
         //(avoid taking multiple times the same value if there wasn't a new value published on the server yet)
         while(nMeas<=parNrMeasPerStep)
         {
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
           if (checkIsOff(scanDp)) {earlyExit(scanDp); return;}
           delay(0,100);
           time newTimeiMon, newTimevMon;
           newTimeiMon=timeiMon;
           newTimevMon=timevMon;
           while(newTimeiMon<=timeiMon || newTimevMon<=timevMon)
           //wait for the current and voltage values to be updated by the OPC server
           {
             if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
             if (checkIsOff(scanDp)) {earlyExit(scanDp); return;}
             delay(0,100);
             dpGet(supplyDp+"/general/measurement.iMon" + ":_online.._stime", newTimeiMon);
             dpGet(supplyDp+"/general/measurement.vMon" + ":_online.._stime", newTimevMon);
             dpGet(supplyDp+"/general/measurement.iMon", measiMon);
             dpGet(supplyDp+"/general/measurement.vMon", measvMon);
           }
           dynAppend(dynMeasiMon, measiMon);
           dynAppend(dynMeasvMon, measvMon);
           timeiMon=newTimeiMon;
           timevMon=newTimevMon;
           nMeas++;
         }
         measStepStop = getCurrentTime(); //to calculate step duration
         dynAppend(dynStepDuration, period(measStepStop)-period(measStepStart));
         stepDuration = dynAvg(dynStepDuration);
         dpSet(scanDp+".data."+dataDpe+".stepDuration", stepDuration);

         //average and sigma calculation, save results to datapoints
         if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
         if (checkIsOff(scanDp)) {earlyExit(scanDp); return;}
         float meanCurrent=dynAvg(dynMeasiMon);
         float meanVoltage=dynAvg(dynMeasvMon);
         dynAppend(currentData, meanCurrent);
         dynAppend(voltageData, meanVoltage);
         dynAppend(timeCount, period(measStepStart)-period(referenceTime) );
         dpSet(scanDp+".data."+dataDpe+".current", currentData);
         dpSet(scanDp+".data."+dataDpe+".voltage", voltageData);
         dpSet(scanDp+".data."+dataDpe+".time",    timeCount);
         float currentSigma=0;
         float voltageSigma=0;
         for(int i=1; i<=parNrMeasPerStep; i++)
         {
           currentSigma+=(dynMeasiMon[i]-meanCurrent)*(dynMeasiMon[i]-meanCurrent);
           voltageSigma+=(dynMeasvMon[i]-meanVoltage)*(dynMeasvMon[i]-meanVoltage);
         }
         if (parNrMeasPerStep>1)
         {
           currentSigma=sqrt( currentSigma/(parNrMeasPerStep-1) );
           voltageSigma=sqrt( voltageSigma/(parNrMeasPerStep-1) );
         }
         else
         {
           currentSigma=0;
           voltageSigma=0;
         }
         dynAppend(currentSigmaData, currentSigma);
         dynAppend(voltageSigmaData, voltageSigma);
         dpSet(scanDp+".data."+dataDpe+".currentSigma", currentSigmaData);
         dpSet(scanDp+".data."+dataDpe+".voltageSigma", voltageSigmaData);

         dynClear(dynMeasiMon);
         dynClear(dynMeasvMon);

         // print to log
         sprintf(iMonS, "%3.3f", meanCurrent*1.0e6);
         sprintf(vMonS, "%3.3f", meanVoltage);
         sprintf(iMonSigmaS, "%3.3f", currentSigma*1.0e6);
         sprintf(vMonSigmaS, "%3.3f", voltageSigma);
         appendLog(scanDp, "Step 0: V="+vMonS+" V, I="+iMonS+" uA, sigmaV="+vMonSigmaS+" V, sigmaI="+iMonSigmaS+" uA, step duration="+(period(measStepStop)-period(measStepStart))+" s");
         if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
         if (checkIsOff(scanDp)) {earlyExit(scanDp); return;}

         ////////////// Do Steps /////////////////////////////
         for ( int step=1; step<=parN; step++ )
         {
           //int step=1;
           voltage -= parStep;
           appendLog(scanDp, "Step "+step+": applying "+voltage+" V.");
           if( !applySettingFloat(supplyDp+"/source/settings.voltageLevel",
                                  supplyDp+"/source/readback.voltageLevel",
                                  voltage) )
             {
             appendError(scanDp, "Could not set voltage to "+voltage+" V. HV scan aborted.");
             earlyExit(scanDp); return;
             }
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
           if (checkIsOff(scanDp)) {earlyExit(scanDp); return;}

           time timeNextStep;
           dpGet(supplyDp+"/source/readback.voltageLevel" + ":_online.._stime", timeNextStep);
           appendLog(scanDp, "Step "+step+": measuring...");
           measStepStart = getCurrentTime();
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
           if (checkIsOff(scanDp)) {earlyExit(scanDp); return;}
           delay(parDelay);
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
           if (checkIsOff(scanDp)) {earlyExit(scanDp); return;}

           // take repeated measurements
           nMeas=1;
           //make sure first measurement accepted has timestamp > time of switch on.
           //timeiMon, timevMon use in while loop below for this check
           timeiMon = makeTime(year(timeNextStep), month(timeNextStep), day(timeNextStep), hour(timeNextStep), minute(timeNextStep), second(timeNextStep)+parDelay, milliSecond(timeNextStep), daylightsaving(timeNextStep) );
           timevMon = makeTime(year(timeNextStep), month(timeNextStep), day(timeNextStep), hour(timeNextStep), minute(timeNextStep), second(timeNextStep)+parDelay, milliSecond(timeNextStep), daylightsaving(timeNextStep) );

           while(nMeas<=parNrMeasPerStep)
           {
             if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
             if (checkIsOff(scanDp)) {earlyExit(scanDp); return;}
             delay(0,100);
             time newTimeiMon, newTimevMon;
             newTimeiMon=timeiMon;
             newTimevMon=timevMon;
             while(newTimeiMon<=timeiMon || newTimevMon<=timevMon)
             //wait for the current and voltage values to be updated by the OPC server
             {
               if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
               if (checkIsOff(scanDp)) {earlyExit(scanDp); return;}
               delay(0,100);
               dpGet(supplyDp+"/general/measurement.iMon" + ":_online.._stime", newTimeiMon);
               dpGet(supplyDp+"/general/measurement.vMon" + ":_online.._stime", newTimevMon);
               dpGet(supplyDp+"/general/measurement.iMon", measiMon);
               dpGet(supplyDp+"/general/measurement.vMon", measvMon);
             }
             dynAppend(dynMeasiMon, measiMon);
             dynAppend(dynMeasvMon, measvMon);
             timeiMon=newTimeiMon;
             timevMon=newTimevMon;
             nMeas++;
           }
           measStepStop = getCurrentTime();
           dynAppend(dynStepDuration, period(measStepStop)-period(measStepStart));
           stepDuration = dynAvg(dynStepDuration);
           dpSet(scanDp+".data."+dataDpe+".stepDuration", stepDuration);

           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
           if (checkIsOff(scanDp)) {earlyExit(scanDp); return;}
           meanCurrent=dynAvg(dynMeasiMon);
           meanVoltage=dynAvg(dynMeasvMon);
           dynAppend(currentData, meanCurrent);
           dynAppend(voltageData, meanVoltage);
           dynAppend(timeCount, period(measStepStart)-period(referenceTime) );
           dpSet(scanDp+".data."+dataDpe+".current", currentData);
           dpSet(scanDp+".data."+dataDpe+".voltage", voltageData);
           dpSet(scanDp+".data."+dataDpe+".time",    timeCount);
           currentSigma=0;
           voltageSigma=0;
           for(int i=1; i<=parNrMeasPerStep; i++)
           {
             currentSigma+=(dynMeasiMon[i]-meanCurrent)*(dynMeasiMon[i]-meanCurrent);
             voltageSigma+=(dynMeasvMon[i]-meanVoltage)*(dynMeasvMon[i]-meanVoltage);
           }
           if (parNrMeasPerStep>1)
           {
           currentSigma=sqrt( currentSigma/(parNrMeasPerStep-1) );
           voltageSigma=sqrt( voltageSigma/(parNrMeasPerStep-1) );
           }
           else
           {
             currentSigma=0;
             voltageSigma=0;
           }
           dynAppend(currentSigmaData, currentSigma);
           dynAppend(voltageSigmaData, voltageSigma);
           dpSet(scanDp+".data."+dataDpe+".currentSigma", currentSigmaData);
           dpSet(scanDp+".data."+dataDpe+".voltageSigma", voltageSigmaData);

           dynClear(dynMeasiMon);
           dynClear(dynMeasvMon);

           sprintf(iMonS, "%3.3f", meanCurrent*1.0e6);
           sprintf(vMonS, "%3.3f", meanVoltage);
           sprintf(iMonSigmaS, "%3.3f", currentSigma*1.0e6);
           sprintf(vMonSigmaS, "%3.3f", voltageSigma);
           appendLog(scanDp, "Step "+step+": V="+vMonS+" V, I="+iMonS+" uA, sigmaV="+vMonSigmaS+" V, sigmaI="+iMonSigmaS+" uA, step duration="+(period(measStepStop)-period(measStepStart))+" s");
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
           if (checkIsOff(scanDp)) {earlyExit(scanDp); return;}

         }//do voltage steps


      ////////////////////////////////// GENTLE RAMP DOWN //////////////////////////////////////////////////////////////////////////////////////////////////////

         ////////////// Do Steps /////////////////////////////
         appendLog(scanDp, "Starting ramp down.");
         for ( int step=1; step<=parN; step++ )
         {
           //int step=1;
           voltage += parStep;

           if( !applySettingFloat(supplyDp+"/source/settings.voltageLevel",
                                  supplyDp+"/source/readback.voltageLevel",
                                  voltage) )
             {
             appendError(scanDp, "Could not set voltage to "+voltage+" V. HV scan aborted.");
             earlyExit(scanDp); return;
             }
           appendLog(scanDp, "Ramp down: "+voltage+" V.");
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
           if (checkIsOff(scanDp)) {earlyExit(scanDp); return;}
           delay(parDelay);
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
           if (checkIsOff(scanDp)) {earlyExit(scanDp); return;}
         }





      ////////////////////////////////// SWITCH OFF //////////////////////////////////////////////////////////////////////////////////////////////////////
         if( !applySettingBool(supplyDp+"/general/settings.onOff",
                               supplyDp+"/general/readback.onOff",
                               FALSE) )
         {
           appendError(scanDp, "Could not switch off the source. Check the source!");
           earlyExit(scanDp); return;
         }
         appendLog(scanDp, "Source switched off.");
         if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

   }  //for(outs)


  //////// 4) SAVE DATA TO FILE ////////////////////////////////////////////////////////////////////



/*
  //temporary
  delay(5);
  appendLog(scanDp, "end of testing. Switching off.");
  applySettingBool(supplyDp+"/general/settings.onOff",
                      supplyDp+"/general/readback.onOff",
                      FALSE) ;
*/

  /////// LAST) WRITE SCAN DATAPOINT ELEMENTS //////////////////////////////////////
  //normal exit
  appendLog(scanDp, "HV scan completed. Normal exit.");
  dpSetWait(scanDp+".scan.endTime",  getCurrentTime());
  dpSetWait(scanDp+".scan.ongoing",   FALSE);
  dpSetWait(scanDp+".scan.triggered", FALSE);

}
