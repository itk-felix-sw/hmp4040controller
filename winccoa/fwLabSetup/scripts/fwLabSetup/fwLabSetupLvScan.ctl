// $License: NOLICENSE
//--------------------------------------------------------------------------------
/**
  @file $relPath
  @copyright $copyright
  @author ressegotti
*/

//--------------------------------------------------------------------------------
// used libraries (#uses)

//--------------------------------------------------------------------------------
// variables and constants

//--------------------------------------------------------------------------------
/**
*/
#uses "fwLabSetup/fwLabSetup.ctl"

main()
{
  dyn_string dpsLvScan = dpNames("*", "fwLabSetupLvScan");

  if (dynlen(dpsLvScan)>0)
  {
    for (int i=1; i<=dynlen(dpsLvScan); i++)
    {
      if(dpConnect("scanTriggeredCB", FALSE, dpsLvScan[i]+".scan.triggered") == 0)
      {
        DebugTN("INFO: LvScan manager dpConnected to "+dpsLvScan[i]);
      }
      else
      {
        DebugTN("Error: LvScan manager could not dpConnect to "+dpsLvScan[i]);
      }
    }

  }
}

int maxCount = 70; //maximum nr. of readings in applySetting** functions

bool applySettingFloat(string settingDp, string readbackDp, float val, string scanDp)
{
  bool initialCheckTriggered = checkTriggered(scanDp);
  float rbVal;
  //try more times and check (from the reaback) that the setting was accepted by the device
  bool ok = false;
  int count=0;
  while ( !ok && count<maxCount )
    {
    if (!checkTriggered(scanDp) && initialCheckTriggered) return ok;
    if (count%30 == 0) dpSetWait(settingDp, val); //resend commands every approx. 15 seconds
    dpGet(readbackDp, rbVal);
    if (fabs(val-rbVal) <= fabs(val*0.001)) ok=TRUE;  //need to use <= (not <) to work also with 0 setting
    //if (val==rbVal) ok=TRUE;
    count++;
    if(!ok) delay(0,500);  //wait 0,5 s to give time the readback to get updated
    }
  return ok;
}

bool applySettingBool(string settingDp, string readbackDp, bool val, string scanDp)
{
  bool initialCheckTriggered = checkTriggered(scanDp);
  bool rbVal;
  //try more times and check (from the readback) that the setting was accepted by the device
  bool ok = false;
  int count=0;
  while ( !ok && count<maxCount )
    {
    if (!checkTriggered(scanDp) && initialCheckTriggered) return ok;
    if (count%30 == 0) dpSetWait(settingDp, val); //resend commands every approx. 15 seconds
    dpGet(readbackDp, rbVal);
    if (val==rbVal) ok=TRUE;
    count++;
    if(!ok) delay(0,500);  //wait 0,5 s to give time the readback to get updated
    }
  return ok;
}

bool applySettingInt(string settingDp, string readbackDp, int val, string scanDp)
{
  bool initialCheckTriggered = checkTriggered(scanDp);
  int rbVal;
  //try more times and check (from the readback) that the setting was accepted by the device
  bool ok = false;
  int count=0;
  while ( !ok && count<maxCount )
    {
    if (!checkTriggered(scanDp) && initialCheckTriggered) return ok;
    if (count%30 == 0) dpSetWait(settingDp, val); //resend commands every approx. 15 seconds
    dpGet(readbackDp, rbVal);
    if (val==rbVal) ok=TRUE;
    count++;
    if(!ok) delay(0,500);  //wait 0,5 s to give time the readback to get updated
    }
  return ok;
}

bool applySettingString(string settingDp, string readbackDp, string val, string scanDp)
{
  bool initialCheckTriggered = checkTriggered(scanDp);
  string rbVal;
  //try more times and check (from the readback) that the setting was accepted by the device
  bool ok = false;
  int count=0;
  while ( !ok && count<maxCount )
    {
    if (!checkTriggered(scanDp) && initialCheckTriggered) return ok;
    if (count%30 == 0) dpSetWait(settingDp, val); //resend commands every approx. 15 seconds
    dpGet(readbackDp, rbVal);
    if (val==rbVal) ok=TRUE;
    count++;
    if(!ok) delay(0,500);  //wait 0,5 s to give time the readback to get updated
    }
  return ok;
}

void appendLog(string scanDp, string line)
{
  dyn_string logg;
  dpGet(scanDp+".scan.log", logg);
  string now = formatTime("%d/%m/%y %H:%M:%S", getCurrentTime());
  line = now+": "+line;
  dynAppend(logg, line); //append at the end
  //dynInsertAt(logg, line, 1);  //insert at the beginning
  dpSetWait(scanDp+".scan.log", logg);
}

void appendError(string scanDp, string line)
{
  dyn_string err;
  dpGet(scanDp+".scan.errors", err);
  string now = formatTime("%d/%m/%y %H:%M:%S", getCurrentTime());
  line = now+": "+line;
  dynAppend(err, line); //append at the end
  //dynInsertAt(err, line, 1);  //insert at the beginning
  dpSetWait(scanDp+".scan.errors", err);
}

bool checkTriggered(string scanDp)
{
  bool triggered;
  dpGet(scanDp+".scan.triggered", triggered);
  return triggered;
}

bool checkIsOff(string scanDp)
{
    bool isOn=true;
    string supplyDp;
    dpGet(scanDp+".supplyDatapoint", supplyDp);
    dpGet(supplyDp+"/actual.isOn", isOn);

  return !isOn;
}

void checkErrors(string scanDp)
{
  bool vTrip=0, iTrip=0, Trip=0, fuseTrip=0;
  int vTripCnt=0, iTripCnt=0, TripCnt=0, fuseTripCnt=0;
  string dp;
  string supplyDp;
  dpGet(scanDp+".supplyDatapoint", supplyDp);

  bool ongoing = true;

  //continue checking for errors as long as the current scan is ongoing
  //don't need to check if some of the errors are invalid (i.e. not supported by this power supply model)
  //because in that case they are FALSE and don't print any error message
  while(ongoing)
  {
    dp=supplyDp + "/actual.vTrip";
    if(dpExists(dp)) dpGet(dp, vTrip);

    dp=supplyDp + "/actual.iTrip";
    if(dpExists(dp)) dpGet(dp, iTrip);

    dp=supplyDp + "/actual.trip";
    if(dpExists(dp)) dpGet(dp, Trip);

    dp=supplyDp + "/actual.fuseTrip";
    if(dpExists(dp)) dpGet(dp, fuseTrip);

    //do "appendError" only at first occurrence
    //(not to fill the error log with tens of lines with same error)
    if(vTrip)
      {
      vTripCnt++;
      if (vTripCnt==1)
        appendError(scanDp, "Error: voltage tripped!");
      }
    if(iTrip)
      {
      iTripCnt++;
      if (iTripCnt==1)
        appendError(scanDp, "Error: current tripped!");
      }
    if(Trip)
      {
      TripCnt++;
      if (TripCnt==1)
        appendError(scanDp, "Error: channel tripped!");
      }
    if(fuseTrip)
      {
      fuseTripCnt++;
      if (fuseTripCnt==1)
        appendError(scanDp, "Error: fuse tripped!");
      }

    //reset counter to zero if trip error goes away
    if(!vTrip)
      {
      vTripCnt=0;
      }
    if(!iTrip)
      {
      iTripCnt=0;
      }
    if(!Trip)
      {
      TripCnt=0;
      }
    if(!fuseTrip)
      {
      fuseTripCnt=0;
      }


    delay(1);
    dpGet(scanDp+".scan.ongoing", ongoing);
  }
}

void earlyExit(string scanDp)
{
  appendLog(scanDp, "Stopping LV scan. Scan not completed.");
  //SWITCH OFF? gently or simply "off"?

/*
  //gently switch off
  string supplyDp;
  float current, parInitialI, parStep;
  int parDelay;
  dpGet(scanDp+".supplyDatapoint", supplyDp);
  dpGet(scanDp+".parameters.initialCurrent", parInitialI);
  dpGet(scanDp+".parameters.currentStep", parStep);
  dpGet(supplyDp+"/readbackSettings.i0", current);  //get initial value for the ramp down
  dpGet(scanDp+".parameters.delay", parDelay);       //s

  bool isOn;
  dpGet(supplyDp+"/actual.isOn", isOn);
  if (!isOn)   appendLog(scanDp, "Source already off. Skipping ramp down. Scan not completed.");
  else
  {
    appendLog(scanDp, "Starting ramp down.");
    while ( current>parInitialI )
    {
      //int step=1;
      delay(parDelay);  //if delay is here: it waits at current voltage level (whatever it is) before starting ramp down, it doesn't wait at the last point (typically zero V)
      current -= parStep;

      if( !applySettingFloat(supplyDp+"/settings.i0",
                             supplyDp+"/readbackSettings.i0",
                             current, scanDp) )
      {
        appendError(scanDp, "Could not set current to "+current+" A or scan stopped. LV scan aborted.");
        earlyExit(scanDp); return;
      }
        appendLog(scanDp, "Ramp down: "+current+" A.");

      //quit ramping if source becomes off for any reason
      dpGet(supplyDp+"/actual.isOn", isOn);
      if (!isOn)
      {
        appendLog(scanDp, "Source found off. Quitting ramp down.");
        break;
      }

    }

    if( !applySettingBool(supplyDp+"/settings.onOff",
        supplyDp+"/actual.isOn",
        FALSE, scanDp) )
      {
        appendError(scanDp, "Could not switch off the source or scan stopped. Check the source!");
        earlyExit(scanDp); return;
      }
    appendLog(scanDp, "Source off. Scan not completed.");
  }
*/

  //"immediate" switch off
  string supplyDp;
  dpGet( scanDp+".supplyDatapoint", supplyDp);
  if( !applySettingBool(supplyDp+"/settings.onOff",
                        supplyDp+"/actual.isOn",
                        FALSE, scanDp) )
  {
    appendError(scanDp, "Could not switch off the source or scan stopped. Check the source!");
  }
  else
    appendLog(scanDp, "Source switched off.");
  //end switch off


  dpSetWait(scanDp+".scan.endTime",  getCurrentTime());
  dpSetWait(scanDp+".scan.ongoing",   FALSE);
  dpSetWait(scanDp+".scan.triggered", FALSE);
}//earlyExit


scanTriggeredCB(string dp, bool ongoing)
{
  string scanDp;
  string supplyDp;
  int ret;

  scanDp = dpSubStr(dp, DPSUB_SYS_DP);
  dpGet( scanDp+".supplyDatapoint", supplyDp);

  ///////// CLEAN SCAN LOG AND ERRORS /////////////////////////////////////////////
  bool triggered;
  dpGet(scanDp+".scan.triggered", triggered);
  if (!triggered) return;

  dpSetWait(scanDp+".scan.errors",     makeDynString());
  dpSetWait(scanDp+".scan.log",        makeDynString());

  if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
  appendLog(scanDp, "Starting LV scan.");

  ////////// 1) READ SCAN PARAMETERS //////////////////////////////////////////////
  bool   parVprot;
  bool   parIprot;
  string parVprotMode;
  float  parVprotLevel;
  float  parIprotLevel;
  float  parInitialI;
  float  parStep;
  int    parN;
  int    parDelay;
  float  parFinalI;
  int    parNrMeasPerStep;
  float  parV0;

  appendLog(scanDp, "Reading scan parameters.");

  dpGet(scanDp+".parameters.vProtection",             parVprot);       //true/false
  dpGet(scanDp+".parameters.iProtection",             parIprot);       //true/false
  dpGet(scanDp+".parameters.vProtectionMode",         parVprotMode);   //MEASured|PROTected
  dpGet(scanDp+".parameters.vProtectionLevel",        parVprotLevel);  //V
  dpGet(scanDp+".parameters.iProtectionLevel",        parIprotLevel);  //A
  dpGet(scanDp+".parameters.initialCurrent",          parInitialI);    //A
  dpGet(scanDp+".parameters.currentStep",             parStep);        //A
  dpGet(scanDp+".parameters.numberOfSteps",           parN);
  dpGet(scanDp+".parameters.delay",                   parDelay);       //s
  dpGet(scanDp+".parameters.finalCurrent",            parFinalI);      //A
  dpGet(scanDp+".parameters.nrOfMeasurementsPerStep", parNrMeasPerStep);
  dpGet(scanDp+".parameters.v0",                      parV0);          //V

  if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

  /////// 2) WRITE SCAN DATAPOINT ELEMENTS //////////////////////////////////////
  dpSetWait(scanDp+".data.singleOut.current", makeDynFloat());
  dpSetWait(scanDp+".data.singleOut.voltage", makeDynFloat());
  dpSetWait(scanDp+".data.singleOut.voltageLoad", makeDynFloat());
  dpSetWait(scanDp+".data.singleOut.currentSigma", makeDynFloat());
  dpSetWait(scanDp+".data.singleOut.voltageSigma", makeDynFloat());
  dpSetWait(scanDp+".data.singleOut.voltageLoadSigma", makeDynFloat());
  dpSetWait(scanDp+".data.singleOut.stepDuration", 0);
  dpSetWait(scanDp+".data.singleOut.time", makeDynFloat());
  dpSetWait(scanDp+".scan.ongoing",           TRUE);
  dpSetWait(scanDp+".scan.startTime",         getCurrentTime());
  dpSetWait(scanDp+".scan.endTime",        0);

  float impedance;
  dpGet(supplyDp+"/settings.cableImpedance" , impedance);
  dpSetWait(scanDp+".data.singleOut.cableImpedance", impedance);

  /////// start "dpConnect" on errors /////
  startThread("checkErrors", scanDp);

  /////// 3a) SEND *RST ////////////////////////////////////////////////////////
  if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
  dyn_dyn_string ddsDeviceHierarchy;
  dyn_string exceptionInfo;
  fwLabSetup_getHierarchy(supplyDp, ddsDeviceHierarchy, exceptionInfo);  //supplyDp is the DP of the CHANNEL, we need the DP of the PS for the *RST command
  if( 0 < dynlen(exceptionInfo) )
  {
    fwExceptionHandling_display(exceptionInfo);
    appendError(scanDp, "Not able to determine power supply datapoint to send *RST command.");
    earlyExit(scanDp); return;
    return;
  }
  dpSetWait(ddsDeviceHierarchy[2][1]+"/commands.reset", TRUE);
  appendLog(scanDp, "*RST sent.");
  delay(1);
  if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

  //range?

  /////// 3b) SET PROTECTION ////////////////////////////////////////////////////
  //apply current/voltage/voltage mode protection only if the relevant DPs exists AND are VALID

  if(dpExists(supplyDp+"/settings.vProtection"))
  {
    bool invalid;
    dpGet(supplyDp+"/readbackSettings.vProtection:_online.._invalid", invalid);
    if(!invalid)
    {
      if( !applySettingBool(supplyDp+"/settings.vProtection",
                      supplyDp+"/readbackSettings.vProtection",
                      parVprot, scanDp) )
      {
      appendError(scanDp, "Could not apply voltage protection setting or scan stopped. LV scan aborted.");
      earlyExit(scanDp); return;
      }
    appendLog(scanDp, "Voltage protection set to "+parVprot+".");
    }
  }

  if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

  if(dpExists(supplyDp+"/settings.iProtection"))
  {
    bool invalid;
    dpGet(supplyDp+"/readbackSettings.iProtection:_online.._invalid", invalid);
    if(!invalid)
    {
    if( !applySettingBool(supplyDp+"/settings.iProtection",
                    supplyDp+"/readbackSettings.iProtection",
                    parIprot, scanDp) )
    {
    appendError(scanDp, "Could not apply current protection setting or scan stopped. LV scan aborted.");
    earlyExit(scanDp); return;
    }
    appendLog(scanDp, "Current protection set to "+parIprot+".");
    }
  }

  if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

  if(dpExists(supplyDp+"/settings.vProtectionMode"))
  {
    bool invalid;
    dpGet(supplyDp+"/readbackSettings.vProtectionMode:_online.._invalid", invalid);
    if(!invalid)
    {
      if( !applySettingString(supplyDp+"/settings.vProtectionMode",
                      supplyDp+"/readbackSettings.vProtectionMode",
                      parVprotMode, scanDp) )
      {
      appendError(scanDp, "Could not apply voltage protection mode setting or scan stopped. LV scan aborted.");
      earlyExit(scanDp); return;
      }
      appendLog(scanDp, "Voltage PROTECTION MODE set to "+parVprotMode+".");
    }
  }

  if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

  //apply voltage protection level if
  //- at least one of vProtection (set to true) or vProtectionMode datapoint exists
  //- vProtectionLevel datapoint exists AND is valid
  if( (dpExists(supplyDp+"/settings.vProtection") && parVprot)  ||
      (dpExists(supplyDp+"/settings.vProtectionMode"))
     )
  {
    if(dpExists(supplyDp+"/settings.vProtectionLevel"))
    {
      bool invalid;
      dpGet(supplyDp+"/readbackSettings.vProtectionLevel:_online.._invalid", invalid);
      if(!invalid)
      {
        if( !applySettingFloat(supplyDp+"/settings.vProtectionLevel",
                        supplyDp+"/readbackSettings.vProtectionLevel",
                        parVprotLevel, scanDp) )
        {
          appendError(scanDp, "Could not apply voltage protection level setting or scan stopped. LV scan aborted.");
          earlyExit(scanDp); return;
        }
        float voltageRb;
        dpGet(supplyDp+"/readbackSettings.vProtectionLevel", voltageRb);
        string v0S=voltageRb;
        //sprintf(i0S, "%3.1f", voltageRb*1.0e6);
        appendLog(scanDp, "Voltage protection level set to "+v0S+" V.");
      }
    }
  }

  if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

  //apply current protection level if
  //- iProtection datapoint exists and is set to true
  //- iProtectionLevel datapoint exists AND is valid
  if( (dpExists(supplyDp+"/settings.iProtection") && parIprot)
    )
  {
    if(dpExists(supplyDp+"/settings.iProtectionLevel"))
    {
      bool invalid;
      dpGet(supplyDp+"/readbackSettings.iProtectionLevel:_online.._invalid", invalid);
      if(!invalid)
      {
        if( !applySettingFloat(supplyDp+"/settings.iProtectionLevel",
                        supplyDp+"/readbackSettings.iProtectionLevel",
                        parIprotLevel, scanDp) )
        {
          appendError(scanDp, "Could not apply current protection level setting or scan stopped. LV scan aborted.");
          earlyExit(scanDp); return;
        }
        float currentRb;
        dpGet(supplyDp+"/readbackSettings.iProtectionLevel", currentRb);
        string i0S=currentRb;
        //sprintf(i0S, "%3.1f", currentRb*1.0e6);
        appendLog(scanDp, "Current protection level set to "+i0S+" A.");
      }
    }
  }

  if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

  ///////////// 3c) set V0 /////////////////////////////////////////////////////////////
         if( !applySettingFloat(supplyDp+"/settings.v0",
                                supplyDp+"/readbackSettings.v0",
                                parV0, scanDp) )
           {
             appendError(scanDp, "Could not set V0 to "+parV0+" or scan stopped. LV scan aborted.");
             earlyExit(scanDp); return;
           }
         appendLog(scanDp, "V0 set to "+parV0+".");
         if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

  if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

  //////// 4) RAMP ////////////////////////////////////////////////////////////////////
      string dataDpe="singleOut";
      float current=parInitialI;

      ////////////////////////////////// RAMP UP //////////////////////////////////////////////////////////////////////////////////////////////////////
         ///////// Set initial Current /////////////////////////////
         if( !applySettingFloat(supplyDp+"/settings.i0",
                                supplyDp+"/readbackSettings.i0",
                                parInitialI, scanDp) )
           {
             appendError(scanDp, "Could not set the initial current to "+parInitialI+" or scan stopped. LV scan aborted.");
             earlyExit(scanDp); return;
           }
         appendLog(scanDp, "Initial current "+parInitialI+" A set.");
         if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

         /////// Switch On //////////////////////////////////
         if( !applySettingBool(supplyDp+"/settings.onOff",
                               supplyDp+"/actual.isOn",
                               TRUE, scanDp) )
         {
         appendError(scanDp, "Could not switch on the LV channel or scan stopped. LV scan aborted.");
         earlyExit(scanDp); return;
         }
         appendLog(scanDp, "LV channel switched on.");
         if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

         time timeOn;
         dpGet(supplyDp+"/actual.isOn" + ":_online.._stime", timeOn);


         ////////////// Take measurements at step 0 //////////////////////////
         time measStepStart, measStepStop;  //to calculate step duration
         time referenceTime; //time "zero"
         float voltage = parInitialI;
         dyn_float currentData, voltageData, voltageLoadData;           //to contain data to be written in dps *.data.*.current/voltage
         dyn_int timeCount;                            //to contain data to be written in dps *.data.*.time
         dyn_float currentSigmaData, voltageSigmaData, voltageLoadSigmaData; //to contain data to be written in dps *.data.*.currentSigma/voltageSigma
         string iMonS, vMonS, vLoadS, iMonSigmaS, vMonSigmaS, vLoadSigmaS;

         appendLog(scanDp, "Step 0: measuring...");
         measStepStart = getCurrentTime();
         referenceTime = measStepStart;
         if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
         if (checkIsOff(scanDp)) { appendError(scanDp, "Source found unexpectedly off. Quitting LV scan."); earlyExit(scanDp); return;}
         delay(parDelay);
         if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
         if (checkIsOff(scanDp)) { appendError(scanDp, "Source found unexpectedly off. Quitting LV scan."); earlyExit(scanDp); return;}

         //new
         int nMeas=1;

         float stepDuration;
         dyn_float dynStepDuration;
         time  timeiMon, timevMon, timevLoad;
         float measiMon, measvMon, measvLoad;
         dyn_float dynMeasiMon, dynMeasvMon, dynMeasvLoad;  //sets of measurements for this voltage step, will be used to calculate average value

         //make sure first measurement accepted has timestamp > of time of switch on:
         //timeiMon, timevMon, timevLoad will be used in while loop below for this check
         timeiMon = makeTime(year(timeOn), month(timeOn), day(timeOn), hour(timeOn), minute(timeOn), second(timeOn)+parDelay, milliSecond(timeOn), daylightsaving(timeOn) );
         timevMon = timeiMon;
         timevLoad = timeiMon;

         //take repeated measurements for this voltage step,
         //verifying that each new measurement has a different timestamp
         //(avoid taking multiple times the same value if there wasn't a new value published on the server yet)
         while(nMeas<=parNrMeasPerStep)
         {
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
           if (checkIsOff(scanDp)) { appendError(scanDp, "Source found unexpectedly off. Quitting LV scan."); earlyExit(scanDp); return;}
           delay(0,100);
           time newTimeiMon, newTimevMon, newTimevLoad;
           newTimeiMon=timeiMon;
           newTimevMon=timevMon;
           newTimevLoad=timevLoad;
           while(newTimeiMon<=timeiMon || newTimevMon<=timevMon || newTimevLoad<=timevLoad)
           //wait for the current and voltage values to be updated by the OPC server
           {
             if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
             if (checkIsOff(scanDp)) { appendError(scanDp, "Source found unexpectedly off. Quitting LV scan."); earlyExit(scanDp); return;}
             delay(0,100);
             dpGet(supplyDp+"/actual.iMon" + ":_online.._stime", newTimeiMon);
             dpGet(supplyDp+"/actual.vMon" + ":_online.._stime", newTimevMon);
             dpGet(supplyDp+"/actual.vLoad" + ":_online.._stime", newTimevLoad);
             dpGet(supplyDp+"/actual.iMon", measiMon);
             dpGet(supplyDp+"/actual.vMon", measvMon);
             dpGet(supplyDp+"/actual.vLoad", measvLoad);
           }
           dynAppend(dynMeasiMon, measiMon);
           dynAppend(dynMeasvMon, measvMon);
           dynAppend(dynMeasvLoad, measvLoad);
           timeiMon=newTimeiMon;
           timevMon=newTimevMon;
           timevLoad=newTimevLoad;
           nMeas++;
         }
         measStepStop = getCurrentTime(); //to calculate step duration
         dynAppend(dynStepDuration, period(measStepStop)-period(measStepStart));
         stepDuration = dynAvg(dynStepDuration);
         dpSet(scanDp+".data."+dataDpe+".stepDuration", stepDuration);

         //average and sigma calculation, save results to datapoints
         if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
         if (checkIsOff(scanDp)) { appendError(scanDp, "Source found unexpectedly off. Quitting LV scan."); earlyExit(scanDp); return;}
         float meanCurrent=dynAvg(dynMeasiMon);
         float meanVoltage=dynAvg(dynMeasvMon);
         float meanVoltageLoad=dynAvg(dynMeasvLoad);
         dynAppend(currentData, meanCurrent);
         dynAppend(voltageData, meanVoltage);
         dynAppend(voltageLoadData, meanVoltageLoad);
         dynAppend(timeCount, period(measStepStart)-period(referenceTime) );
         dpSet(scanDp+".data."+dataDpe+".current", currentData);
         dpSet(scanDp+".data."+dataDpe+".voltage", voltageData);
         dpSet(scanDp+".data."+dataDpe+".voltageLoad", voltageLoadData);
         dpSet(scanDp+".data."+dataDpe+".time",    timeCount);
         float currentSigma=0;
         float voltageSigma=0;
         float voltageLoadSigma=0;
         for(int i=1; i<=parNrMeasPerStep; i++)
         {
           currentSigma+=(dynMeasiMon[i]-meanCurrent)*(dynMeasiMon[i]-meanCurrent);
           voltageSigma+=(dynMeasvMon[i]-meanVoltage)*(dynMeasvMon[i]-meanVoltage);
           voltageLoadSigma+=(dynMeasvLoad[i]-meanVoltageLoad)*(dynMeasvLoad[i]-meanVoltageLoad);
         }
         if (parNrMeasPerStep>1)
         {
           currentSigma=sqrt( currentSigma/(parNrMeasPerStep-1) );
           voltageSigma=sqrt( voltageSigma/(parNrMeasPerStep-1) );
           voltageLoadSigma=sqrt( voltageLoadSigma/(parNrMeasPerStep-1) );
         }
         else
         {
           currentSigma=0;
           voltageSigma=0;
           voltageLoadSigma=0;
         }

         dynAppend(currentSigmaData, currentSigma);
         dynAppend(voltageSigmaData, voltageSigma);
         dynAppend(voltageLoadSigmaData, voltageLoadSigma);
         dpSet(scanDp+".data."+dataDpe+".currentSigma", currentSigmaData);
         dpSet(scanDp+".data."+dataDpe+".voltageSigma", voltageSigmaData);
         dpSet(scanDp+".data."+dataDpe+".voltageLoadSigma", voltageLoadSigmaData);

         dynClear(dynMeasiMon);
         dynClear(dynMeasvMon);
         dynClear(dynMeasvLoad);

         // print to log
         sprintf(iMonS, "%3.3f", meanCurrent);
         sprintf(vMonS, "%3.3f", meanVoltage);
         sprintf(vLoadS, "%3.3f", meanVoltageLoad);
         sprintf(iMonSigmaS, "%3.3f", currentSigma);
         sprintf(vMonSigmaS, "%3.3f", voltageSigma);
         sprintf(vLoadSigmaS, "%3.3f", voltageLoadSigma);
         appendLog(scanDp, "Step 0: I="+iMonS+" A, V="+vMonS+" V, Vload="+vLoadS+" V,\nsigmaI="+iMonSigmaS+" A, sigmaV="+vMonSigmaS+" V, sigmaVload="+vLoadSigmaS+" V,\nstep duration="+(period(measStepStop)-period(measStepStart))+" s");
         if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
         if (checkIsOff(scanDp)) { appendError(scanDp, "Source found unexpectedly off. Quitting LV scan."); earlyExit(scanDp); return;}

         //switch off between steps
         if( !applySettingBool(supplyDp+"/settings.onOff",
                        supplyDp+"/actual.isOn",
                        FALSE, scanDp) )
        {
          appendError(scanDp, "Could not switch off the source or scan stopped. Check the source! LV scan aborted.");
          earlyExit(scanDp); return;
        }
        if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}


         ////////////// Do Steps /////////////////////////////
         for ( int step=1; step<=parN; step++ )
         {
           //int step=1;
           current += parStep;
           appendLog(scanDp, "Step "+step+": applying "+current+" A.");
           if( !applySettingFloat(supplyDp+"/settings.i0",
                                  supplyDp+"/readbackSettings.i0",
                                  current, scanDp) )
             {
             appendError(scanDp, "Could not set current to "+current+" A or scan stopped. LV scan aborted.");
             earlyExit(scanDp); return;
             }
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

           //switch on for next step
           if( !applySettingBool(supplyDp+"/settings.onOff",
                                 supplyDp+"/actual.isOn",
                                 TRUE, scanDp) )
           {
             appendError(scanDp, "Could not switch on the source or scan stopped. Check the source! LV scan aborted.");
             earlyExit(scanDp); return;
           }
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

           if (checkIsOff(scanDp)) { appendError(scanDp, "Source found unexpectedly off. Quitting LV scan."); earlyExit(scanDp); return;}

           dpGet(supplyDp+"/actual.isOn" + ":_online.._stime", timeOn);
           time timeNextStep = timeOn;
           appendLog(scanDp, "Step "+step+": measuring...");
           measStepStart = getCurrentTime();
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
           if (checkIsOff(scanDp)) { appendError(scanDp, "Source found unexpectedly off. Quitting LV scan."); earlyExit(scanDp); return;}
           delay(parDelay);
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
           if (checkIsOff(scanDp)) { appendError(scanDp, "Source found unexpectedly off. Quitting LV scan."); earlyExit(scanDp); return;}

           // take repeated measurements
           nMeas=1;
           //make sure first measurement accepted has timestamp > time of switch on.
           //timeiMon, timevMon, timevLoad use in while loop below for this check
           timeiMon = makeTime(year(timeNextStep), month(timeNextStep), day(timeNextStep), hour(timeNextStep), minute(timeNextStep), second(timeNextStep)+parDelay, milliSecond(timeNextStep), daylightsaving(timeNextStep) );
           timevMon = makeTime(year(timeNextStep), month(timeNextStep), day(timeNextStep), hour(timeNextStep), minute(timeNextStep), second(timeNextStep)+parDelay, milliSecond(timeNextStep), daylightsaving(timeNextStep) );
           timevLoad = makeTime(year(timeNextStep), month(timeNextStep), day(timeNextStep), hour(timeNextStep), minute(timeNextStep), second(timeNextStep)+parDelay, milliSecond(timeNextStep), daylightsaving(timeNextStep) );

           while(nMeas<=parNrMeasPerStep)
           {
             if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
             if (checkIsOff(scanDp)) { appendError(scanDp, "Source found unexpectedly off. Quitting LV scan."); earlyExit(scanDp); return;}
             delay(0,100);
             time newTimeiMon, newTimevMon, newTimevLoad;
             newTimeiMon=timeiMon;
             newTimevMon=timevMon;
             newTimevLoad=timevLoad;
             while(newTimeiMon<=timeiMon || newTimevMon<=timevMon || newTimevLoad<=timevLoad)
             //wait for the current and voltage values to be updated by the OPC server
             {
               if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
               if (checkIsOff(scanDp)) { appendError(scanDp, "Source found unexpectedly off. Quitting LV scan."); earlyExit(scanDp); return;}
               delay(0,100);
               dpGet(supplyDp+"/actual.iMon" + ":_online.._stime", newTimeiMon);
               dpGet(supplyDp+"/actual.vMon" + ":_online.._stime", newTimevMon);
               dpGet(supplyDp+"/actual.vLoad" + ":_online.._stime", newTimevLoad);
               dpGet(supplyDp+"/actual.iMon", measiMon);
               dpGet(supplyDp+"/actual.vMon", measvMon);
               dpGet(supplyDp+"/actual.vLoad", measvLoad);
             }
             dynAppend(dynMeasiMon, measiMon);
             dynAppend(dynMeasvMon, measvMon);
             dynAppend(dynMeasvLoad, measvLoad);
             timeiMon=newTimeiMon;
             timevMon=newTimevMon;
             timevLoad=newTimevLoad;
             nMeas++;
           }
           measStepStop = getCurrentTime();
           dynAppend(dynStepDuration, period(measStepStop)-period(measStepStart));
           stepDuration = dynAvg(dynStepDuration);
           dpSet(scanDp+".data."+dataDpe+".stepDuration", stepDuration);

           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
           if (checkIsOff(scanDp)) { appendError(scanDp, "Source found unexpectedly off. Quitting LV scan."); earlyExit(scanDp); return;}
           meanCurrent=dynAvg(dynMeasiMon);
           meanVoltage=dynAvg(dynMeasvMon);
           meanVoltageLoad=dynAvg(dynMeasvLoad);
           dynAppend(currentData, meanCurrent);
           dynAppend(voltageData, meanVoltage);
           dynAppend(voltageLoadData, meanVoltageLoad);
           dynAppend(timeCount, period(measStepStart)-period(referenceTime) );
           dpSet(scanDp+".data."+dataDpe+".current", currentData);
           dpSet(scanDp+".data."+dataDpe+".voltage", voltageData);
           dpSet(scanDp+".data."+dataDpe+".voltageLoad", voltageLoadData);
           dpSet(scanDp+".data."+dataDpe+".time",    timeCount);
           currentSigma=0;
           voltageSigma=0;
           voltageLoadSigma=0;
           for(int i=1; i<=parNrMeasPerStep; i++)
           {
             currentSigma+=(dynMeasiMon[i]-meanCurrent)*(dynMeasiMon[i]-meanCurrent);
             voltageSigma+=(dynMeasvMon[i]-meanVoltage)*(dynMeasvMon[i]-meanVoltage);
             voltageLoadSigma+=(dynMeasvLoad[i]-meanVoltageLoad)*(dynMeasvLoad[i]-meanVoltageLoad);
           }
           if (parNrMeasPerStep>1)
           {
             currentSigma=sqrt( currentSigma/(parNrMeasPerStep-1) );
             voltageSigma=sqrt( voltageSigma/(parNrMeasPerStep-1) );
             voltageLoadSigma=sqrt( voltageLoadSigma/(parNrMeasPerStep-1) );
           }
           else
           {
             currentSigma=0;
             voltageSigma=0;
             voltageLoadSigma=0;
           }

           dynAppend(currentSigmaData, currentSigma);
           dynAppend(voltageSigmaData, voltageSigma);
           dynAppend(voltageLoadSigmaData, voltageLoadSigma);
           dpSet(scanDp+".data."+dataDpe+".currentSigma", currentSigmaData);
           dpSet(scanDp+".data."+dataDpe+".voltageSigma", voltageSigmaData);
           dpSet(scanDp+".data."+dataDpe+".voltageLoadSigma", voltageLoadSigmaData);

           dynClear(dynMeasiMon);
           dynClear(dynMeasvMon);
           dynClear(dynMeasvLoad);

           sprintf(iMonS, "%3.3f", meanCurrent);
           sprintf(vMonS, "%3.3f", meanVoltage);
           sprintf(vLoadS, "%3.3f", meanVoltageLoad);
           sprintf(iMonSigmaS, "%3.3f", currentSigma);
           sprintf(vMonSigmaS, "%3.3f", voltageSigma);
           sprintf(vLoadSigmaS, "%3.3f", voltageLoadSigma);
           appendLog(scanDp, "Step "+step+": I="+iMonS+" A, V="+vMonS+" V, Vload="+vLoadS+" V,\nsigmaI="+iMonSigmaS+" A, sigmaV="+vMonSigmaS+" V, sigmaVLoad="+vLoadSigmaS+" V,\nstep duration="+(period(measStepStop)-period(measStepStart))+" s");
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
           if (checkIsOff(scanDp)) { appendError(scanDp, "Source found unexpectedly off. Quitting LV scan."); earlyExit(scanDp); return;}

           //switch off between steps
           if( !applySettingBool(supplyDp+"/settings.onOff",
                                 supplyDp+"/actual.isOn",
                                 FALSE, scanDp) )
           {
             appendError(scanDp, "Could not switch off the source or scan stopped. Check the source! LV scan aborted.");
             earlyExit(scanDp); return;
           }
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}


         }//do voltage steps

/*
      ////////////////////////////////// GENTLE RAMP DOWN //////////////////////////////////////////////////////////////////////////////////////////////////////

         ////////////// Do Steps /////////////////////////////
         appendLog(scanDp, "Starting ramp down.");
         for ( int step=1; step<=parN; step++ )
         {
           //int step=1;
           current -= parStep;

           if( !applySettingFloat(supplyDp+"/settings.i0",
                                  supplyDp+"/readbackSettings.i0",
                                  current, scanDp) )
             {
             appendError(scanDp, "Could not set current to "+current+" A or scan stopped. LV scan aborted.");
             earlyExit(scanDp); return;
             }
           appendLog(scanDp, "Ramp down: "+current+" A.");
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
           if (checkIsOff(scanDp)) { appendError(scanDp, "Source found unexpectedly off. Quitting LV scan."); earlyExit(scanDp); return;}
           delay(parDelay);
           if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}
           if (checkIsOff(scanDp)) { appendError(scanDp, "Source found unexpectedly off. Quitting LV scan."); earlyExit(scanDp); return;}
         }
*/

/*
      ////////////////////////////////// SWITCH OFF //////////////////////////////////////////////////////////////////////////////////////////////////////
         if( !applySettingBool(supplyDp+"/settings.onOff",
                               supplyDp+"/actual.isOn",
                               FALSE, scanDp) )
         {
           appendError(scanDp, "Could not switch off the LV channel or scan stopped. Check the power supply!");
           earlyExit(scanDp); return;
         }
         appendLog(scanDp, "LV channel switched off.");
         if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}

*/
        //at this point the source must already be off
        if (checkIsOff(scanDp)) appendLog(scanDp, "LV channel switched off.");
        else
        {
          //if it's not found off, give it another try....
          if( !applySettingBool(supplyDp+"/settings.onOff",
                                 supplyDp+"/actual.isOn",
                                 FALSE, scanDp) )
           {
             appendError(scanDp, "Could not switch off the source or scan stopped. Check the source! LV scan aborted.");
             earlyExit(scanDp); return;
           }
        }
        if (!checkTriggered(scanDp)) {earlyExit(scanDp); return;}



  //////// 4) SAVE DATA TO FILE ////////////////////////////////////////////////////////////////////



/*
  //temporary
  delay(5);
  appendLog(scanDp, "end of testing. Switching off.");
  applySettingBool(supplyDp+"/general/settings.onOff",
                      supplyDp+"/actual.isOn",
                      FALSE, scanDp) ;
*/

  /////// LAST) WRITE SCAN DATAPOINT ELEMENTS //////////////////////////////////////
  //normal exit
  appendLog(scanDp, "LV scan completed. Normal exit.");
  dpSetWait(scanDp+".scan.endTime",  getCurrentTime());
  dpSetWait(scanDp+".scan.ongoing",   FALSE);
  dpSetWait(scanDp+".scan.triggered", FALSE);

}
