// $License: NOLICENSE
//--------------------------------------------------------------------------------
/**
  @file $relPath
  @copyright $copyright
  @author ressegotti
*/

//--------------------------------------------------------------------------------
// used libraries (#uses)

//--------------------------------------------------------------------------------
// variables and constants

//--------------------------------------------------------------------------------
/**
*/


main()
{
  dyn_string dpsSwInterlock = dpNames("*", "fwLabSetupSwInterlock");

  if (dynlen(dpsSwInterlock)==0) return;

  //DPCONNECTS TO MAKE INTERLOCK WORK
  for (int i=1; i<=dynlen(dpsSwInterlock); i++) //loop over fwLabSetupSwInterlock DPEs
  {
    dyn_string sourceDpes;
    dpGet(dpsSwInterlock[i]+".sourceDpes", sourceDpes);

    if (dynlen(sourceDpes)==0) continue;

    for (int j=1; j<=dynlen(sourceDpes); j++)  //loop over .sourceDpes
    {
      //pass also swInterlock datapoint, and element i of .sourceDpes
      if(dpConnectUserData("sourceDpeChanged", dpsSwInterlock[i]+";"+j, sourceDpes[j]) == 0)
      {
	    if (dpSubStr(sourceDpes[j], DPSUB_DP_EL) != "ExampleDP_BarTrend.x1")
          DebugTN("INFO: SwInterlock manager dpConnected to "+sourceDpes[j]);
      }
      else
      {
        DebugTN("Error: SwInterlock manager could not dpConnect to "+sourceDpes[j]);
      }
    }//loop over .sourceDpes
  }//loop over fwLabSetupSwInterlock DPEs


  //DPCONNECTS TO WRITE DATA TO SwInterlockWrite DP (to get archiving by the OpcServer)
  for (int i=1; i<=dynlen(dpsSwInterlock); i++) //loop over fwLabSetupSwInterlock DPEs
  {
    dyn_string sourceDpes;
    dyn_string elements = makeDynString(".sourceDpes", ".targetDpes", ".thresholdTypes",
                                        ".thresholdValues", ".targetValues", ".active", ".triggeredHistory",
                                        ".triggered");
    if (dpSubStr(dpsSwInterlock[i], DPSUB_DP)=="dummySwInterlock") continue;

      for (int k=1; k<=dynlen(elements); k++)
      {
        if(dpConnectUserData("elementChanged", dpsSwInterlock[i]+";"+k, dpsSwInterlock[i]+elements[k]) == 0)
        {
           DebugTN("INFO elementChanged: SwInterlock manager dpConnected to "+dpsSwInterlock[i]+elements[k]);
        }
        else
        {
          DebugTN("Error elementChanged: SwInterlock manager could not dpConnect to "+dpsSwInterlock[i]+elements[k]);
        }
      }//loop over elements
  }//loop over fwLabSetupSwInterlock DPEs

}


sourceDpeChanged(string userData, string dp, anytype val)
{
  //DebugTN("running sourceDpeChanged, dp changed: ", dp);
  dyn_string split = strsplit(userData, ";");
  string swInterlockDp=split[1];
  int elem=split[2];

  //goal of dummySwInterlock is just for consistency:
  //ensure that CTRL manager -num 12 starts up when first interlock is saved by user
  if ( dpSubStr(swInterlockDp, DPSUB_DP) == "dummySwInterlock" )    return;

  dyn_bool active;
  dpGet(swInterlockDp+".active", active);


  dyn_string thrType;
  dyn_float threshold;
  dpGet(swInterlockDp+".thresholdTypes", thrType);
  dpGet(swInterlockDp+".thresholdValues", threshold);
  string condition = val+thrType[elem]+threshold[elem]; //e.g. 15<20

  bool retVal;

  string func =
    "bool main() {\n"+
    "bool ret = FALSE;\n"+
    "if ("+condition+") ret=TRUE;\n"+
    "return ret;\n"+
    "}";

  int eval = evalScript(retVal, func);


  if (!retVal)  //interlock condition not met -> release lock if any, set triggered=false
  {
    //Run this code also if interlock is not set active
    //(it may have been disabled while the interlock was triggered and DPEs locked,
    //in that case DPEs must get unlocked)
   dyn_string targetDpes;
   dpGet(swInterlockDp+".targetDpes", targetDpes);

   //unlock Dpe if locked
   bool locked;
   dpGet(targetDpes[elem]+":_lock._original._locked",locked);
   if (locked!=0)
     dpSet(targetDpes[elem]+":_lock._original._locked",0);

   //set triggered to FALSE if previously TRUE
   dyn_bool triggered;
   dpGet(swInterlockDp+".triggered", triggered);
   if (triggered[elem]!=false)
   {
     triggered[elem]=false;
     dpSet(swInterlockDp+".triggered", triggered);
   }

   //set .direction to WENT if previously CAME
   dyn_string dir;
   dpGet(swInterlockDp+".direction", dir);
   if (dir[elem]=="CAME")
   {
     dir[elem]="WENT";
     dpSet(swInterlockDp+".direction", dir);
   }
  }

  if (retVal)  //interlock condition met!! activate interlock
  {
   if (!active[elem])
   {
     return;  //exit if interlock is not active
   }

   //set triggered
   dyn_bool triggered;
   dpGet(swInterlockDp+".triggered", triggered);
   if (triggered[elem]!=true)
   {
     triggered[elem]=true;
     dpSet(swInterlockDp+".triggered", triggered);
   }

   //save timestamp
   time now = getCurrentTime();
   dyn_string timestamps;
   dpGet(swInterlockDp+".triggeredHistory", timestamps);
   string newValue = now;
   timestamps[elem] = newValue+";"+timestamps[elem];
   dpSet(swInterlockDp+".triggeredHistory", timestamps);

   dyn_string targetDpes;
   dpGet(swInterlockDp+".targetDpes", targetDpes);
   dyn_string targetValues;
   dpGet(swInterlockDp+".targetValues", targetValues);
   DebugTN("SOWFTARE INTERLOCK TRIGGERED! "+targetDpes[elem]+" set to "+targetValues[elem]+"\n(condition "+dp+thrType[elem]+threshold[elem]+" detected.)");

   //set direction
   dyn_string dir;
   dpGet(swInterlockDp+".direction", dir);
   if (dir[elem]!="CAME")
   {
     dir[elem]="CAME";
     dpSet(swInterlockDp+".direction", dir);
   }

   //set target value to targetDpe!!
   int count=0;
   bool ok=false;
   while (count<10)
   {
     dpSetWait(targetDpes[elem],targetValues[elem]);
     string set;
     dpGet(targetDpes[elem], set);
     if (set==targetValues[elem])
     {
       ok=true;
       break;
     }
     else  delay(0,100);
     count++;
   }

   if (!ok)
   {
     //warn user there's a problem
     DebugTN("SOFTWARE INTERLOCK FAILED TO SET "+targetDpes[elem]+" to "+targetValues[elem]+
             "!!\n(Condition "+dp+thrType[elem]+threshold[elem]+" detected)\nHence DPE was not locked. PLEASE CHECK your setup!");
     dyn_string errlog;
     dpGet(swInterlockDp+".errorLog", errlog);
     string now = formatTime("%d/%m/%y %H:%M:%S", getCurrentTime());
     dynInsertAt(errlog,
                 now+": SOFTWARE INTERLOCK FAILED TO SET "+targetDpes[elem]+" to "+targetValues[elem]+
                 "!!\n(Condition "+dp+thrType[elem]+threshold[elem]+" detected)\nHence interlock action DID NOT WORK and target DPE was not locked. PLEASE CHECK your setup!",
                 1);
     dpSet(swInterlockDp+".errorLog", errlog);

     return;
   }

   //lock Dpe if not locked
   //locking is done ONLY IF targetValue successfully applied to targetDpe
   bool locked;
   dpGet(targetDpes[elem]+":_lock._original._locked",locked);
   if (locked!=1)
     dpSet(targetDpes[elem]+":_lock._original._locked",1);

   //check if variable is locked, warn user if not
   bool locked;
   dpGet(targetDpes[elem]+":_lock._original._locked",locked);
   if (locked!=1)
   {
     DebugTN("SOFTWARE INTERLOCK FAILED TO LOCK "+targetDpes[elem]+"!!\nThe variable is still writeable!!");
     dyn_string errlog;
     dpGet(swInterlockDp+".errorLog", errlog);
     string now = formatTime("%d/%m/%y %H:%M:%S", getCurrentTime());
     dynInsertAt(errlog,
                 now+": SOFTWARE INTERLOCK FAILED TO LOCK "+targetDpes[elem]+"!!\nThe variable is still WRITEABLE!!",
                 1);
     dpSet(swInterlockDp+".errorLog", errlog);
   }


  }//if (retVal)

}

elementChanged(string userData, string dp, anytype val)
{
  //DebugTN("running elementChanged, dp changed: ", dp);
  dyn_string split = strsplit(userData, ";");
  string swInterlockDp=split[1];
  int elem=split[2];

  //goal of dummySwInterlock is just for consistency:
  //ensure that CTRL manager -num 12 starts up when first interlock is saved by user
  if ( dpSubStr(swInterlockDp, DPSUB_DP) == "dummySwInterlock" )    return;

  dyn_bool active;
  dyn_string thrType;
  dyn_float threshold;
  dyn_string sourceDpes;
  dyn_string targetDpes;
  dyn_string targetValues;
  dyn_string history;
  dyn_bool triggered;

  dpGet(swInterlockDp+".active", active);
  dpGet(swInterlockDp+".thresholdTypes", thrType);
  dpGet(swInterlockDp+".thresholdValues", threshold);
  dpGet(swInterlockDp+".sourceDpes", sourceDpes);
  dpGet(swInterlockDp+".targetDpes", targetDpes);
  dpGet(swInterlockDp+".targetValues", targetValues);
  dpGet(swInterlockDp+".triggeredHistory", history);
  dpGet(swInterlockDp+".triggered", triggered);

  string swInterlockOpc = "swInterlockWrite";

  // NAME must be the FIRST dp to be set, ACTIVE,TRIGGERED must be the LAST dp to be set
  // because opcServer uses info in "name" to archive data with correct dp name
  // and trending panel looks BACK for ALL interlock details for each "active","triggered" point to be represented in the plot
  dpSetWait(swInterlockOpc+".name", dpSubStr(swInterlockDp, DPSUB_DP),
            swInterlockOpc+".sourceDpes", sourceDpes,
            swInterlockOpc+".targetDpes", targetDpes,
            swInterlockOpc+".thresholdTypes", thrType,
            swInterlockOpc+".thresholdValues", threshold,
            swInterlockOpc+".targetValues", targetValues,
            //swInterlockOpc+".triggeredHistory", history,
            swInterlockOpc+".active", active,
            swInterlockOpc+".triggered", triggered);

  //==================================================================
  //If interlock was deactivated while triggered, then:
  //1) set .direction to WENT if previously CAME
  //2) set triggered to FALSE and unlock DPE
  //Viceversa if interlock was activated:
  //1) force to check the interlock condition (don't wait for a change in sourceDpe)

  if (strpos(dp,".active")<0) return;

  dyn_bool active;
  dpGet(swInterlockDp+".active", active);
  dyn_string dir;
  dpGet(swInterlockDp+".direction", dir);
  dyn_string trig;
  dpGet(swInterlockDp+".triggered", trig);
  dyn_string sourceDpes;
  dpGet(swInterlockDp+".sourceDpes", sourceDpes);
  for (int itl=1; itl<=dynlen(active); itl++)
  {
     if (!active[itl] && dir[itl]=="CAME")
     {
       dir[itl]="WENT";
       bool locked;
       dpGet(targetDpes[itl]+":_lock._original._locked",locked);
       if (locked)    dpSet(targetDpes[itl]+":_lock._original._locked",0);  //unlock dpe if interlock is set inactive while triggered
       if (trig[itl])
       {
         trig[itl]=0;
         dpSet(swInterlockDp+".triggered",trig);  //set triggered to FALSE if interlock condition is satisfiged but active is changed to FALSE
       }
     }
     if (active[itl])  //force to evaluate interlock condition on sourceDpe
       {
         anytype newval;
         dpGet(sourceDpes[itl], newval);
         sourceDpeChanged(swInterlockDp+";"+itl, sourceDpes[itl], newval);
         //update direction of local variable dir of this script after sourceDpeChanged run
         dyn_string dir2;
         dpGet(swInterlockDp+".direction", dir2);
         dir[itl]=dir2[itl];
       }

   }//for loop over itl
   dpSet(swInterlockDp+".direction", dir);
}
