// $License: NOLICENSE
//--------------------------------------------------------------------------------
/**
  @file $relPath
  @copyright $copyright
  @author ressegotti
*/

//--------------------------------------------------------------------------------
// Libraries used (#uses)

//--------------------------------------------------------------------------------
// Variables and Constants


//--------------------------------------------------------------------------------
//@public members
//--------------------------------------------------------------------------------


//--------------------------------------------------------------------------------
//@private members
//--------------------------------------------------------------------------------

/**This function executes a ramp on a dp of fwLabSetupSourceMeter type (and dp/dpts of its family),
        applying voltage in steps from the current value (either off or on at any initiale voltage value).
        The sourcemeter DP, the amplitude of the voltage step and the delay time between steps are passed as argument
        to the function.
        If the sourcemeter is initially OFF, the permission to switch it on is defined by argument allowTurnOn
        (if se to FALSE, the operation will be aborted whe the sourcemeter is initially off).
        The ramping is quitted when abort is set to TRUE.

   @par Modification History
        xx/yy/zz Babbo Natale   Added functionality for something
                                                        This makes it possible to do something

   @par Constraints

   @par Usage
        Public

   @par PVSS managers
        VISION

   @param exceptionInfo details of any exceptions are placed in here.
 */
void fwLabSetupSourceMeter_initializeRampDp(string smDp)
{
    string rampDp = dpSubStr(smDp, DPSUB_DP)+"Ramp"; //without system name, otherwise dpCreate() doesn't accept it
    bool rampDpExists = true;

    //Create ramp DP if it doesn't exist already
    if (!dpExists(rampDp))
    {
      rampDpExists = false;
      DebugTN("Creating datapoint "+rampDp+"...");
      int res = dpCreate(rampDp, "fwLabSetupSourceMeterRamp");
      if (res==0)  DebugTN("Datapoint "+rampDp+" created.");
      else         DebugTN("Error: could not create datapoint "+rampDp);


      //add dpFunction for finalVoltage and units
      if (res==0)
      {
        //get initial values from templateRamp
        string exampleDp = getSystemName()+"templateRamp";
        float vStep, del;
        dpGet(exampleDp+".voltageStep", vStep);
        dpGet(exampleDp+".delay", del);

        dpSet(rampDp+".voltageStep", vStep);
        dpSet(rampDp+".delay", del);
        dpSet(rampDp+".abort", false);
        dpSet(rampDp+".rampOngoing", false);
        dpSetUnit(rampDp+".delay", dpGetUnit(exampleDp+".delay"));
      }


    }
    rampDp = dpSubStr(smDp, DPSUB_SYS) + rampDp; //add system name
}


/**This function executes a ramp on a dp of fwLabSetupSourceMeter type (and dp/dpts of its family),
        applying voltage in steps from the current value (either off or on at any initiale voltage value).
        The sourcemeter DP, the amplitude of the voltage step and the delay time between steps are passed as argument
        to the function.
        If the sourcemeter is initially OFF, the permission to switch it on is defined by argument allowTurnOn
        (if se to FALSE, the operation will be aborted whe the sourcemeter is initially off).
        The ramping is quitted when ex (exit) is set to TRUE.
        The target value is forced to be NEGATIVE!

   @par Modification History
        xx/yy/zz Babbo Natale   Added functionality for something
                                                        This makes it possible to do something

   @par Constraints

   @par Usage
        Public

   @par PVSS managers
        VISION

   @param exceptionInfo details of any exceptions are placed in here.
 */
int fwLabSetupSourceMeter_rampToVoltage(string smDp, float &targetV, float vStep, float del,
                                        dyn_string &exceptionInfo, bool allowTurnOn=FALSE)
{
  //returns 0 if normal exit (ramp finished), otherwise -1 if scan was interrupted for some reason.

  //ramp in steps to another voltage (larger or smaller) than the current one.
  //If the source is ON, starts ramp from current voltage value.
  //If the source is OFF, starts ramp from zero volts.

  //exceptionInfo return value exceptionInfo[3] =
  //0 is "another scan/ramp ongoing"
  //1 is "can't switch on"
  //2 is "can't switch off"
  //3 is "can't apply voltage"
  //4 is "source is already off"

  //check that ramp DP exists
  if (!dpExists(smDp+"Ramp"))
  {
    DebugTN("Ramp datapoint for sourcemeter "+smDp+" does not exist.\nfwLabSetupSourceMeter_initializeRampDp() must be run before doing any ramping.\nAborting.");
    return -1;
  }

  //initialize exit variable to .abort DPE
  bool ex=false;

  //check that no HV scan or ramp is currently ongoing
  bool rampOngoing=false;
  bool hvScanOngoing=false;

  if (dpExists(smDp+"HvScan"))  dpGet(smDp+"HvScan.scan.ongoing", hvScanOngoing);
  if (dpExists(smDp+"Ramp"))    dpGet(smDp+"Ramp.rampOngoing", rampOngoing);
  else fwLabSetupSourceMeter_initializeRampDp(smDp);

  if (rampOngoing || hvScanOngoing)
  {
  		fwException_raise(exceptionInfo,
									"ERROR",
									"fwLabSetupSourceMeter_rampToVoltage(): Another ramp or HV scan is ongoing.\nAborting ramp with "+smDp+".",
									"0");
       return -1;
  }

  //ok, start ramp
  dpSet(smDp+"Ramp.rampOngoing", 1);

  float initialV;

  dpSet(smDp+"Ramp.voltageStep", vStep);
  dpSet(smDp+"Ramp.delay",       del);

  if (targetV>0)
  {
    targetV=-1.00*fabs(targetV);
  }

  float vMon;
  dpGet(smDp+"/general/measurement.vMon", vMon);
  bool isOn;
  dpGet(smDp+"/general/readback.onOff", isOn);
  initialV=vMon;

  dpGet(smDp+"Ramp.abort", ex);
  if(ex)
  {
    dpSet(smDp+"Ramp.rampOngoing", 0);
    dpSet(smDp+"Ramp.abort", false);
    return -1;
  }


  if (!isOn)
  {
    //if the source is initially off and turn-on not allowed, return
    if(!allowTurnOn)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      return -1;
    }

    dpGet(smDp+"Ramp.abort", ex);
    if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }

    //the source is initially off. Set the voltageLevel to zero volts, then switch on.
    //So the ramp will be FROM ZERO TO TARGET VALUE
    initialV=0.000;

    if( !fwLabSetupSourceMeter_applySettingFloat(smDp+"/source/settings.voltageLevel",
                               smDp+"/source/readback.voltageLevel",
                               0.000) )
    {
 			fwException_raise( exceptionInfo,
                				 "ERROR",
								         "fwLabSetupSourceMeter_rampToVoltage(): Could not apply voltage V=0.\nABORTING ramp to target state.",
								         "3");
      dpSet(smDp+"Ramp.rampOngoing", 0);
      return -1;
    }

    dpGet(smDp+"Ramp.abort", ex);
    if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }

    if( !fwLabSetupSourceMeter_applySettingBool(smDp+"/general/settings.onOff",
                               smDp+"/general/readback.onOff",
                               TRUE) )
   {
     fwException_raise( exceptionInfo,
                				 "ERROR",
								         "fwLabSetupSourceMeter_rampToVoltage(): Could not switch ON the source!\nABORTING ramp to target state.",
								         "1");
     dpSet(smDp+"Ramp.rampOngoing", 0);
     return -1;
   }

    dpGet(smDp+"Ramp.abort", ex);
    if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }

  }

  dpGet(smDp+"Ramp.abort", ex);
  if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }

  float vSet=initialV;
  float step;
  float direction=1.000;
  if ( targetV>initialV ) step=fabs(vStep);  //positive steps to go from smaller to larger voltage
  else
  {
    step=-1.0*fabs(vStep);  //negative steps to go from larger to smaller voltage
    direction=-1.000;
  }

  vSet+=step;

  while(direction*(vSet-targetV)<0)  //continue until next voltage setting would overcome target voltage
  {
    dpGet(smDp+"Ramp.abort", ex);
    if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }

   delay(floor(del), (del-floor(del))*1000);
   //wait for actual vMon to change accordingly
   dpGet(smDp+"Ramp.abort", ex);
   if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }

   if( !fwLabSetupSourceMeter_applySettingFloat(smDp+"/source/settings.voltageLevel",
                                smDp+"/source/readback.voltageLevel",
                                vSet) )
     {
       fwException_raise( exceptionInfo,
                				 "ERROR",
								         "fwLabSetupSourceMeter_rampToVoltage(): Applying voltage step with V="+vSet+" V failed.\nABORTING ramp to target voltage.",
								         "3");
       dpSet(smDp+"Ramp.rampOngoing", 0);
       return -1;
     }


   vSet+=step;
  }

  if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }

    vSet=targetV;
    delay(floor(del), (del-floor(del))*1000);
   //wait for actual vMon to change accordingly

    if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }

   if( !fwLabSetupSourceMeter_applySettingFloat(smDp+"/source/settings.voltageLevel",
                                smDp+"/source/readback.voltageLevel",
                                vSet) )
     {
       fwException_raise( exceptionInfo,
                				 "ERROR",
								         "fwLabSetupSourceMeter_rampToVoltage(): Applying voltage step with V="+vSet+" V failed.\nABORTING ramp to target voltage.",
								         "3");
       dpSet(smDp+"Ramp.rampOngoing", 0);
       return -1;
     }


      dpSet(smDp+"Ramp.rampOngoing", 0);
      return 0;
}

int fwLabSetupSourceMeter_rampToOff(string smDp, float vStep, float del, dyn_string &exceptionInfo)
{
  //returns 0 if normal exit (ramp finished), otherwise -1 if scan was interrupted for some reason.

  //ramp in steps from ON to 0V, then switch OFF

  //exceptionInfo return value exceptionInfo[3] =
  //0 is "another scan/ramp ongoing"
  //1 is "can't switch on"
  //2 is "can't switch off"
  //3 is "can't apply voltage"
  //4 is "source is already off"

  //check that ramp DP exists
  if (!dpExists(smDp+"Ramp"))
  {
    DebugTN("Ramp datapoint for sourcemeter "+smDp+" does not exist.\nfwLabSetupSourceMeter_initializeRampDp() must be run before doing any ramping.\nAborting.");
    return -1;
  }

  //initialize exit variable to .abort DPE
  bool ex=false;

  //check that no HV scan or ramp is currently ongoing
  bool rampOngoing=false;
  bool hvScanOngoing=false;

  if (dpExists(smDp+"HvScan"))  dpGet(smDp+"HvScan.scan.ongoing", hvScanOngoing);
  if (dpExists(smDp+"Ramp"))  dpGet(smDp+"Ramp.rampOngoing", rampOngoing);
  else fwLabSetupSourceMeter_initializeRampDp(smDp);

  if (rampOngoing || hvScanOngoing)
  {
                fwException_raise(exceptionInfo, "ERROR",
                                  "fwLabSetupSourceMeter_rampToVoltage(): Another ramp or HV scan is ongoing.\nAborting ramp with "+smDp+".",
                                   "0");
       return -1;
  }


  //ok, start ramp
  dpSet(smDp+"Ramp.rampOngoing", 1);

  dpSet(smDp+"Ramp.voltageStep", vStep);
  dpSet(smDp+"Ramp.delay",       del);

  float vMon;
  dpGet(smDp+"/general/measurement.vMon", vMon);
  bool isOn;
  dpGet(smDp+"/general/readback.onOff", isOn);

  dpGet(smDp+"Ramp.abort", ex);
  if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }

  if (!isOn)
  {
    /*
    dyn_float df;
    dyn_string ds;
    ChildPanelOnCentralReturn("vision/MessageWarning2", "Warning",
                              makeDynString("$1:The source is OFF.\nSwitch on the source?", "$2:Yes", "$3:No"),
                              df, ds);
    if (!(dynlen(df)>0 && df[1]==1)) return;

    */

    fwException_raise( exceptionInfo,  "WARNING", "fwLabSetupSourceMeter_rampToOff(): The source is already OFF. Aborting.",
                                                                         "4");
    dpSet(smDp+"Ramp.rampOngoing", 0);
    return -1;
  }

  float vSet=vMon;
  float step;
  if (vSet<0) step=fabs(vStep);  //positive steps to go from negative voltage to zero
  else if (vSet>0) step=-1.0*fabs(vStep);  //negative steps to go from positive voltage to zero

  dpGet(smDp+"Ramp.abort", ex);
  if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }

  vSet+=step;

  while(fabs(vSet)>(fabs(step)*0.5))  //continue until next voltage setting would overcome zero volts
  {
    dpGet(smDp+"Ramp.abort", ex);
    if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }

   delay(floor(del), (del-floor(del))*1000);

   dpGet(smDp+"Ramp.abort", ex);
   if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }

   //wait for actual vMon to change accordingly
   if( !fwLabSetupSourceMeter_applySettingFloat(smDp+"/source/settings.voltageLevel",
                                smDp+"/source/readback.voltageLevel",
                                vSet) )
     {
      fwException_raise( exceptionInfo, "ERROR",
                         "fwLabSetupSourceMeter_rampToVoltage(): Could not apply voltage V=0.\nABORTING ramp to off state.",
                         "3");
      dpSet(smDp+"Ramp.rampOngoing", 0);
      return -1;
     }

   dpGet(smDp+"Ramp.abort", ex);
   if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }


   vSet+=step;
  }

  dpGet(smDp+"Ramp.abort", ex);
  if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }

    vSet=0.000;
    delay(floor(del), (del-floor(del))*1000);

    dpGet(smDp+"Ramp.abort", ex);
    if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }

   //wait for actual vMon to change accordingly
   if( !fwLabSetupSourceMeter_applySettingFloat(smDp+"/source/settings.voltageLevel",
                                smDp+"/source/readback.voltageLevel",
                                vSet) )
     {
      fwException_raise( exceptionInfo, "ERROR",
                         "fwLabSetupSourceMeter_rampToVoltage(): Could not apply voltage V=0.\nABORTING ramp to off state.",
                         "3");
       dpSet(smDp+"Ramp.rampOngoing", 0);
       return -1;
     }

    dpGet(smDp+"Ramp.abort", ex);
    if(ex)
    {
      dpSet(smDp+"Ramp.rampOngoing", 0);
      dpSet(smDp+"Ramp.abort", false);
      return -1;
    }

    //switch off
    if( !fwLabSetupSourceMeter_applySettingBool(smDp+"/general/settings.onOff",
                               smDp+"/general/readback.onOff",
                               FALSE) )
     {
       fwException_raise( exceptionInfo, "ERROR",
                         "fwLabSetupSourceMeter_rampToVoltage(): Could not switch off the source!\nThe source is still ON with v="+vSet+" V.",
                         "2");
       dpSet(smDp+"Ramp.rampOngoing", 0);
       return -1;
     }


      dpSet(smDp+"Ramp.rampOngoing", 0);

  return 0;
}

bool fwLabSetupSourceMeter_applySettingFloat(string settingDp, string readbackDp, float val)
{
  float rbVal;
  //try 10 times and check (from the reaback) that the setting was accepted by the device
  bool ok = false;
  int count=0;
  while ( !ok && count<10 )
    {
    int ret = dpSetWait(settingDp, val);
    dpGet(readbackDp, rbVal);
    if (fabs(val-rbVal) <= fabs(val*0.001)) ok=TRUE;  //need to use <= (not <) to work also with 0 setting
    //if (val==rbVal) ok=TRUE;
    count++;
    if(!ok) delay(0,500);  //wait 0,5 s to give time the readback to get updated
    }
  return ok;
}

bool fwLabSetupSourceMeter_applySettingBool(string settingDp, string readbackDp, bool val)
{
  bool rbVal;
  //try 10 times and check (from the readback) that the setting was accepted by the device
  bool ok = false;
  int count=0;
  while ( !ok && count<10 )
    {
    int ret = dpSetWait(settingDp, val);
    dpGet(readbackDp, rbVal);
    if (val==rbVal) ok=TRUE;
    count++;
    if(!ok) delay(0,500);  //wait 0,5 s to give time the readback to get updated
    }
  return ok;
}

bool fwLabSetupSourceMeter_applySettingInt(string settingDp, string readbackDp, int val)
{
  int rbVal;
  //try 10 times and check (from the readback) that the setting was accepted by the device
  bool ok = false;
  int count=0;
  while ( !ok && count<10 )
    {
    int ret = dpSetWait(settingDp, val);
    dpGet(readbackDp, rbVal);
    if (val==rbVal) ok=TRUE;
    count++;
    if(!ok) delay(0,500);  //wait 0,5 s to give time the readback to get updated
    }
  return ok;
}
